import PropTypes from 'prop-types';

const attachment = PropTypes.shape({
  id: PropTypes.number.isRequired,
  post_id: PropTypes.number.isRequired,
  created_at: PropTypes.string.isRequired,
  updated_at: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  uuid: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
});

export default attachment;
