import PropTypes from 'prop-types';

import attachment from './attachment';

const post = PropTypes.shape({
  id: PropTypes.number.isRequired,
  thread_id: PropTypes.number.isRequired,
  created_at: PropTypes.string.isRequired,
  updated_at: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  is_sage: PropTypes.bool.isRequired,
  replies: PropTypes.arrayOf(PropTypes.shape({
    reply_id: PropTypes.number.isRequired,
    reply_thread_id: PropTypes.number.isRequired,
  })).isRequired,
  replies_on: PropTypes.arrayOf(PropTypes.shape({
    post_id: PropTypes.number.isRequired,
    post_thread_id: PropTypes.number.isRequired,
  })).isRequired,
  attachments: PropTypes.arrayOf(attachment).isRequired,
});

export default post;
