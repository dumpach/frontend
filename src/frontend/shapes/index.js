import attachment from './attachment';
import board from './board';
import post from './post';
import section from './section';
import thread from './thread';

export {
  attachment, board, post, section, thread,
};
