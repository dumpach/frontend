import PropTypes from 'prop-types';

const board = PropTypes.shape({
  created_at: PropTypes.string.isRequired,
  updated_at: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  section_id: PropTypes.number.isRequired,
  threads_limit: PropTypes.number.isRequired,
});

export default board;
