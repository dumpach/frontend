import React from 'react';
import Document, {
  Html, Head, Main, NextScript,
} from 'next/document';
import NextSeo from 'next-seo';

import config from '../config';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);

    return {
      ...initialProps,
    };
  }

  render() {
    return (
      <Html lang="en">
        <Head>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
          />
          <link
            rel="stylesheet"
            href="https://necolas.github.io/normalize.css/8.0.1/normalize.css"
          />

          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/static/apple-touch-icon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/static/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/static/favicon-16x16.png"
          />
          <link rel="manifest" href="/static/site.webmanifest" />
        </Head>
        <body>
          <NextSeo config={config.seo({ title: 'Homepage' })} />

          <div className="content">
            <Main />
          </div>
          <NextScript />

          <style>
            {`
              :root {
                --background-color: #fafafa;
                --main-text-color: rgba(0, 0, 0, 0.87);
                --invert-text-color: rgba(255, 255, 255, 0.87);
                --accent-text-color: #f44336;
              }

              * {
                font-family: Roboto, Helvetica, Arial, sans-serif;
              }
              
              html {
                color: var(--main-text-color);
                background-color: var(--background-color);
              }

              ul {
                padding: 0;
                margin: 0;
              }

              li {
                list-style: none;
              }

              @media only screen and (min-width: 0) {
                .content {
                  margin: 0 10px;
                }
              }

              @media only screen and (min-width: 480px) {
                .content {
                  margin: 0 10%;
                }
              }

              @media only screen and (min-width: 768px) {
                .content {
                  margin: 0 20%;
                }
              }

              @media only screen and (min-width: 1920px) {
                .content {
                  margin: 0 30%;
                }
              }    
            `}
          </style>
        </body>
      </Html>
    );
  }
}

export default MyDocument;
