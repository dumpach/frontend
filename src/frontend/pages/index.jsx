import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import NextSeo from 'next-seo';

import { Link } from '../../renderer/routes';
import { getBoards } from '../store/thunks/boards';
import { PageTitle } from '../components/typography';

import { section as sectionShape } from '../shapes';
import config from '../config';

class HomePage extends React.Component {
  static async getInitialProps({ reduxStore }) {
    return reduxStore.dispatch(getBoards());
  }

  render() {
    const { sections } = this.props;

    return (
      <div className="home-page">
        <NextSeo config={config.seo({ title: 'Homepage' })} />

        <PageTitle text="Homepage" />
        <ul>
          {sections.data.map((section, index) => (
            <li
              className={`section ${index % 2 === 0 ? 'left-align' : 'right-align'}`}
              key={section.id}
            >
              <h2 className="section-title">{section.title}</h2>
              {section.boards.map((board) => (
                <Link
                  key={board.id}
                  route={`/boards/${board.id}`}
                  href={`/boards/${board.id}`}
                  params={{ boardId: board.id }}
                >
                  <a href={`/boards/${board.id}`} className="board-link">
                    {`${board.id} - ${board.title}`}
                  </a>
                </Link>
              ))}
            </li>
          ))}
        </ul>

        <style jsx>
          {`
            .section {
              margin-bottom: 1em;
            }

            .left-align {
              text-align: left;
            }

            .right-align {
              text-align: right;
            }

            .section-title {
              font-size: 3em;
              font-weight: 400;
              margin: 0;
            }

            .board-link {
              color: inherit;
              display: block;
              text-decoration: none;
              font-size: 2.125em;
              transition: 0.1s;
            }

            .board-link:hover {
              color: var(--accent-text-color);
              font-size: 2.5em;
            }

            .board-link:focus {
              color: var(--accent-text-color);
              font-size: 2.5em;
            }
          `}
        </style>
      </div>
    );
  }
}

HomePage.propTypes = {
  sections: PropTypes.shape({
    status: PropTypes.string.isRequired,
    data: PropTypes.arrayOf(sectionShape).isRequired,
  }).isRequired,
};

const mapStateToProps = ({ boards }) => ({
  sections: boards,
});

export default connect(mapStateToProps)(HomePage);
