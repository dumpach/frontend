import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import PropTypes from 'prop-types';
import NextSeo from 'next-seo';

import { ThreadForm, ThreadFormToggle, Thread } from '../components/elements';
import { PageTitle } from '../components/typography';
import ErrorPage from './_error';

import { getThread, updateThread } from '../store/thunks/thread';

import { thread as threadShape } from '../shapes';
import config from '../config';

import { IS_FETCHING, IS_NOT_FOUND } from '../store/status';

class ThreadPage extends React.Component {
  state = { isFormOpen: false };

  static async getInitialProps({ reduxStore, query: { boardId, threadId } }) {
    await reduxStore.dispatch(getThread(boardId, threadId));
    // Strange hack for threadId.
    // If you click reply link that points to another thread
    // and move on previous page, threadId doesn't update in router query params.
    return { query: { boardId, threadId } };
  }

  onFormSubmit = async (formData, clearFormCallback) => {
    const {
      updateExistingThread,
      router: {
        query: { boardId, threadId },
      },
    } = this.props;
    await updateExistingThread(boardId, threadId, formData, clearFormCallback);
  };

  toggleForm = () => {
    const { isFormOpen } = this.state;
    this.setState({ isFormOpen: !isFormOpen });
  };

  render() {
    const {
      thread,
      router: {
        query: { boardId, threadId },
      },
    } = this.props;
    const { isFormOpen } = this.state;

    if (thread.status === IS_NOT_FOUND) {
      return <ErrorPage statusCode={404} errorText="Not found" />;
    }

    return (
      <div className="thread-page">
        <NextSeo
          config={config.seo({
            title: thread.data.posts[0].title || `${boardId}:${thread.data.id}`,
            description: thread.data.posts[0].title || null,
            image:
              thread.data.posts[0].attachments.length > 0
                ? `${config.app.uploads.path}/source/${boardId}/${threadId}/${
                  thread.data.posts[0].attachments[0].uuid
                }-${thread.data.posts[0].attachments[0].name}`
                : null,
          })}
        />

        <PageTitle text={`Board - ${boardId}. Thread - ${threadId}`} />
        <ThreadFormToggle onToggle={this.toggleForm} isOpen={isFormOpen} />
        <ThreadForm
          onSubmit={this.onFormSubmit}
          isFetching={thread.status === IS_FETCHING}
          isOpen={isFormOpen}
        />
        <Thread key={thread.data.id} thread={thread.data} />

        <style jsx>
          {`
            .thread-page {
              margin-bottom: 20px;
            }
          `}
        </style>
      </div>
    );
  }
}

ThreadPage.propTypes = {
  thread: PropTypes.shape({
    status: PropTypes.string.isRequired,
    data: threadShape.isRequired,
  }).isRequired,
  router: PropTypes.shape({
    query: PropTypes.shape({ boardId: PropTypes.string.isRequired }).isRequired,
  }).isRequired,
  updateExistingThread: PropTypes.func.isRequired,
};

const mapStateToProps = ({ thread }) => ({
  thread,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ updateExistingThread: updateThread }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(ThreadPage));
