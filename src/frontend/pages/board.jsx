import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import PropTypes from 'prop-types';
import NextSeo from 'next-seo';

import {
  ThreadForm,
  ThreadFormToggle,
  Thread,
  ThreadsPagination,
} from '../components/elements';
import { PageTitle } from '../components/typography';
import ErrorPage from './_error';

import { getThreads } from '../store/thunks/threads';
import { createThread } from '../store/thunks/thread';

import { thread as threadShape } from '../shapes';
import config from '../config';

import { IS_FETCHING, IS_FETCHED, IS_NOT_FOUND } from '../store/status';

class BoardPage extends React.Component {
  state = { isFormOpen: false, pageId: 0 };

  static async getInitialProps({ reduxStore, query: { boardId, pageId } }) {
    return reduxStore.dispatch(getThreads(boardId, pageId));
  }

  componentDidMount = () => {
    const {
      router: {
        query: { pageId = '0' },
      },
    } = this.props;
    this.setState({ pageId });
  };

  onFormSubmit = async (formData, clearFormCallback) => {
    const {
      createNewThread,
      router: {
        query: { boardId },
      },
    } = this.props;
    await createNewThread(boardId, formData, clearFormCallback);
  };

  toggleForm = () => {
    const { isFormOpen } = this.state;
    this.setState({ isFormOpen: !isFormOpen });
  };

  updatePage = (pageId) => {
    this.setState({ pageId });
  };

  render() {
    const {
      threads,
      router: {
        query: { boardId },
      },
    } = this.props;
    const { isFormOpen, pageId } = this.state;

    if (threads.status === IS_NOT_FOUND) {
      return <ErrorPage statusCode={404} errorText="Not found" />;
    }

    return (
      <div className="board-page">
        <NextSeo config={config.seo({ title: boardId })} />

        <PageTitle text={`Board - ${boardId}`} />
        <ThreadFormToggle onToggle={this.toggleForm} isOpen={isFormOpen} />
        <ThreadForm
          onSubmit={this.onFormSubmit}
          isNewThread
          isFetching={threads.status === IS_FETCHING}
          isOpen={isFormOpen}
        />
        <ThreadsPagination
          boardId={boardId}
          pageId={parseInt(pageId, 10)}
          count={threads.count}
          updatePage={this.updatePage}
        />
        <div className="threads">
          {threads.data.map((thread) => (
            <Thread key={thread.id} thread={thread} isPreview />
          ))}
        </div>
        {threads.status === IS_FETCHED ? (
          <ThreadsPagination
            boardId={boardId}
            pageId={parseInt(pageId, 10)}
            count={threads.count}
            updatePage={this.updatePage}
          />
        ) : null}
      </div>
    );
  }
}

BoardPage.propTypes = {
  threads: PropTypes.shape({
    status: PropTypes.string.isRequired,
    data: PropTypes.arrayOf(threadShape).isRequired,
    count: PropTypes.number.isRequired,
    isLastPage: PropTypes.bool.isRequired,
  }).isRequired,
  router: PropTypes.shape({
    query: PropTypes.shape({
      boardId: PropTypes.string.isRequired,
      pageId: PropTypes.string,
    }).isRequired,
  }).isRequired,
  createNewThread: PropTypes.func.isRequired,
};

const mapStateToProps = ({ threads }) => ({
  threads,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ createNewThread: createThread }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(BoardPage));
