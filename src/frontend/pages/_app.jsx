import React from 'react';
import App, { Container } from 'next/app';
import { Provider } from 'react-redux';

import withReduxStore from '../store/withReduxStore';
import { Menu } from '../components/elements';

class MyApp extends App {
  state = { isMenuOpen: false };

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  toggleMenuOpen = () => {
    const { isMenuOpen } = this.state;
    this.setState({ isMenuOpen: !isMenuOpen });
  };

  render() {
    const { isMenuOpen } = this.state;
    const {
      Component, pageProps, reduxStore, router,
    } = this.props;

    return (
      <Container>
        <Provider store={reduxStore}>
          <div className="menu-icon-container">
            {!isMenuOpen ? (
              <i
                tabIndex="0"
                role="button"
                className="material-icons menu-icon"
                onClick={this.toggleMenuOpen}
                onKeyPress={this.toggleMenuOpen}
              >
                menu
              </i>
            ) : null}
          </div>
          {isMenuOpen ? (
            <Menu isOpen={isMenuOpen} toggleOpen={this.toggleMenuOpen} />
          ) : (
            <Component key={router.route} {...pageProps} />
          )}
        </Provider>

        <style jsx>
          {`
            .menu-icon-container {
              position: fixed;
              top: 15px;
              left: 15px;
              outline: none;
            }

            .menu-icon {
              font-size: 1.5em;
              cursor: pointer;
              color: var(--main-text-color);
              outline: none;
            }
            .menu-icon:hover {
              color: var(--accent-text-color);
              transition: 0.1s;
            }
            .menu-icon:focus {
              color: var(--accent-text-color);
              transition: 0.1s;
            }
          `}
        </style>
      </Container>
    );
  }
}

export default withReduxStore(MyApp);
