import React from 'react';
import PropTypes from 'prop-types';

import { PageTitle } from '../components/typography';

class ErrorPage extends React.Component {
  static getInitialProps({ res, err }) {
    /* eslint-disable-next-line no-nested-ternary */
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode };
  }

  render() {
    const { statusCode, errorText = 'Not found' } = this.props;

    const imageId = Math.floor(Math.random() * 2);
    const imageLink = `/static/images/not_found/not_found_${imageId}.png`;

    return (
      <div className="error-page">
        <PageTitle text={errorText} />
        <img className="error-page-image" src={imageLink} alt="Not found" />
        <PageTitle text={statusCode} />

        <style jsx>
          {`
            .error-page {
              display: flex;
              justify-content: center;
              align-items: center;
              height: 100vh;
              flex-direction: column;
            }

            .error-page-image {
              max-width: 50%;
              
              transition: 0.4s;
            }
            .error-page-image:hover {
              max-width: 60%;
            }
          `}
        </style>
      </div>
    );
  }
}

ErrorPage.propTypes = {
  statusCode: PropTypes.number.isRequired,
  errorText: PropTypes.string.isRequired,
};

export default ErrorPage;
