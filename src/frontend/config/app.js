import getConfig from 'next/config';

let NODE_ENV;
let API_HOST;
let API_PORT;

if (process.env !== undefined) {
  // Backend rendering.
  /* eslint-disable prefer-destructuring */
  NODE_ENV = process.env.NODE_ENV;
  API_HOST = process.env.API_HOST;
  API_PORT = process.env.API_PORT;
  /* eslint-enable prefer-destructuring */
} else {
  // Frontend rendering.
  /* eslint-disable prefer-destructuring */
  const publicRuntimeConfig = getConfig().publicRuntimeConfig;
  NODE_ENV = publicRuntimeConfig.NODE_ENV;
  API_HOST = publicRuntimeConfig.API_HOST;
  API_PORT = publicRuntimeConfig.API_PORT;
  /* eslint-enable prefer-destructuring */
}

export const isServer = process.browser === false;
export const env = NODE_ENV;

const development = {
  api: {
    endpoint: isServer ? `http://${API_HOST}:${API_PORT}/api/v1` : '/api/v1',
  },
  uploads: {
    path: '/static/uploads',
  },
  isServer,
};

const test = {
  api: {
    endpoint: isServer ? `http://${API_HOST}:${API_PORT}/api/v1` : '/api/v1',
  },
  uploads: {
    path: '/static/uploads',
  },
  isServer,
};

const production = {
  api: {
    endpoint: isServer ? `http://${API_HOST}:${API_PORT}/api/v1` : '/api/v1',
  },
  uploads: {
    path: '/uploads',
  },
  isServer,
};

const config = {
  development,
  test,
  production,
};

export default config[NODE_ENV];
