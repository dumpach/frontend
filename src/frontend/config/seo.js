// export default { titleTemplate: '%s | Dumpach' };
const SITE_URL = 'https://dumpach.shitcunt.info';

const defaultTitle = 'Dumpach';
const defaultDescription = 'Dump your shit here.';
const defaultImage = `${SITE_URL}/static/logo.png`;
const defaultSep = ' | ';

export default ({
  title, description, image, noindex,
}) => {
  const theTitle = title
    ? (title + defaultSep + defaultTitle).substring(0, 60)
    : defaultTitle;
  const theDescription = description
    ? description.substring(0, 155)
    : defaultDescription;
  const theImage = image || defaultImage;

  return {
    title: theTitle,
    description: theDescription,
    canonical: SITE_URL,
    noindex: noindex === true,
    twitter: {
      cardType: 'summary_large_image',
      site: SITE_URL,
    },
    openGraph: {
      title: theTitle,
      description: theDescription,
      url: SITE_URL,
      type: 'website',
      images: [{ url: theImage, alt: '' }],
      site_name: defaultTitle,
    },
  };
};
