import app from './app';
import seo from './seo';

const config = {
  app,
  seo,
};

export default config;
