import React from 'react';
import PropTypes from 'prop-types';

import config from '../../../config';

import { attachment as attachmentShape } from '../../../shapes';

const PostAttachment = ({ attachment, boardId, threadId }) => (
  <div className="attachment-container">
    <a
      target="blank"
      href={`${config.app.uploads.path}/source/${boardId}/${threadId}/${
        attachment.uuid
      }-${attachment.name}`}
    >
      <img
        className="attachment"
        alt={attachment.name}
        src={`${config.app.uploads.path}/thumb/${boardId}/${threadId}/${
          attachment.uuid
        }-${attachment.name}`}
      />
    </a>

    <style jsx>
      {`
        .attachment {
          max-width: 150px;
          max-height: 150px;
        }
      `}
    </style>
  </div>
);

PostAttachment.propTypes = {
  attachment: attachmentShape.isRequired,
  boardId: PropTypes.string.isRequired,
  threadId: PropTypes.number.isRequired,
};

export default PostAttachment;
