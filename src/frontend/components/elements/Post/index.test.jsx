import React from 'react';
import ReactDOM from 'react-dom';

import Post from '.';

const testPost = {
  attachments: [],
  created_at: '2019-04-07T13:22:36.338Z',
  id: 1,
  is_sage: true,
  text: 'Test',
  thread_id: 1,
  replies: [
    {
      reply_id: 1,
      reply_thread_id: 1,
    },
  ],
  replies_on: [
    {
      post_id: 1,
      post_thread_id: 1,
    },
  ],
  title: 'Test',
  updated_at: '2019-04-07T13:22:36.338Z',
};

describe('Elements', () => {
  describe('Post', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(
        <Post
          post={testPost}
          boardId="b"
          threadRemainedPosts={50}
          key={testPost.id}
          indexInThread={1}
          isPreview
        />,
        div,
      );
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
