import React from 'react';
import ReactDOM from 'react-dom';

import PostAttachment from './attachment';

const testAttachment = {
  created_at: '2019-04-29T10:11:19.828Z',
  id: 1,
  name: 'Test.jpg',
  post_id: 1,
  size: 136089,
  type: 'image/jpeg',
  updated_at: '2019-04-29T10:11:19.828Z',
  uuid: '20ab5d95-813c-4b37-bd11-73eb090677c0',
};

describe('Elements', () => {
  describe('Post', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(
        <PostAttachment attachment={testAttachment} boardId="b" threadId={1} />,
        div,
      );
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
