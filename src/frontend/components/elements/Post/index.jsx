import React from 'react';
import PropTypes from 'prop-types';
import reactStringReplace from 'react-string-replace';

import { Link } from '../../../../renderer/routes';

import PostAttachment from './attachment';
import {
  PostTitle,
  PostMetaText,
  PostText,
  PostReplyLink,
  PostReplyOnLink,
} from '../../typography';

import { post as postShape } from '../../../shapes';

const Post = ({
  post, isPreview, threadRemainedPosts, indexInThread, boardId,
}) => (
  <div id={post.id} className={isPreview ? 'post post-preview' : 'post'}>
    {post.title !== '' ? (
      <PostTitle
        text={
          post.title.length > 150 ? `${post.title.slice(0, 150)}...` : post.title
        }
      />
    ) : null}

    <div className="post-meta-text-container">
      <PostMetaText
        text={`${new Date(post.created_at).toLocaleDateString()}
        ${new Date(post.created_at).toLocaleTimeString()}`}
      />

      <PostMetaText
        text={(
          <Link
            key={`${post.thread_id}/${boardId}`}
            route={`/boards/${boardId}/threads/${post.thread_id}#${post.id}`}
            href={`/boards/${boardId}/threads/${post.thread_id}#${post.id}`}
            params={{ boardId, threadId: post.thread_id }}
          >
            <a
              href={`/boards/${boardId}/threads/${post.thread_id}#${post.id}`}
              className="thread-link"
            >
              {`#${post.id}`}
            </a>
          </Link>
)}
      />

      {isPreview && indexInThread === 1 ? (
        <PostMetaText
          text={(
            <Link
              key={`${post.thread_id}/${boardId}`}
              route={`/boards/${boardId}/threads/${post.thread_id}`}
              href={`/boards/${boardId}/threads/${post.thread_id}`}
              params={{ boardId, threadId: post.thread_id }}
            >
              <a
                href={`/boards/${boardId}/threads/${post.thread_id}`}
                className="thread-link"
              >
                Open
              </a>
            </Link>
)}
        />
      ) : null}

      {!isPreview ? <PostMetaText text={indexInThread} /> : null}
    </div>

    {isPreview && indexInThread === 1 ? (
      <PostMetaText text={`Remained posts: ${threadRemainedPosts}`} />
    ) : null}

    {post.attachments.length > 0 ? (
      <div className="post-attachments-container">
        {post.attachments.map((attachment) => (
          <PostAttachment
            key={attachment.id}
            attachment={attachment}
            boardId={boardId}
            threadId={post.thread_id}
          />
        ))}
      </div>
    ) : null}

    {post.text !== '' ? (
      <div className="post-text-container">
        {post.text.split('\n').map((line, lineIndex) => {
          const answersMatches = line.match(/>>\d+/gm) || [];

          if (answersMatches.length === 0) {
            return (
              /* eslint-disable-next-line react/no-array-index-key */
              <div className="post-text-line-container" key={`${line}-${lineIndex}`}>
                <PostText text={line} />
              </div>
            );
          }

          const answers = answersMatches.map((stringAnswer) =>
            parseInt(stringAnswer.split('>>')[1], 10));

          let lineWithAnswersLinks = line;
          answers.forEach((answer) => {
            const replyOn = post.replies_on.find(
              /* eslint-disable-next-line camelcase */
              (reply_on) => answer === reply_on.post_id,
            );

            if (replyOn === undefined) {
              return;
            }

            lineWithAnswersLinks = reactStringReplace(
              lineWithAnswersLinks,
              `>>${answer}`,
              (_, index) => (
                <PostReplyOnLink
                  key={`${replyOn.post_id}-${replyOn.post_thread_id}-${index}`}
                  boardId={boardId}
                  replyOn={replyOn}
                />
              ),
            );
          });

          return (
            /* eslint-disable-next-line react/no-array-index-key */
            <div className="post-text-line-container" key={`${line}-${lineIndex}`}>
              <PostText text={lineWithAnswersLinks} />
            </div>
          );
        })}
      </div>
    ) : null}

    {post.replies.length > 0 ? (
      <div className="post-replies-container">
        {post.replies.map((reply, index) => (
          <PostReplyLink
            /* eslint-disable-next-line react/no-array-index-key */
            key={`${reply.reply_id}-${reply.reply_thread_id}-${index}`}
            boardId={boardId}
            reply={reply}
          />
        ))}
      </div>
    ) : null}

    <style >
      {`
        .post {
          margin-bottom: 20px;
        }
        .post:last-child {
          margin-bottom: 0;
        }

        .post__focused {
          font-size: 1.5em;
        }
        .post__focused .attachment {
          max-width: 200px;
          max-height: 200px;
        }

        .post-attachments-container {
          display: flex;
          align-items: center;
          justify-content: space-between;
          flex-wrap: wrap;
        }

        .post-text-container {
          margin-bottom: 5px;
        }

        .thread-link {
          color: inherit;
          transition: 0.1s;
        }
        .thread-link:hover {
          color: var(--accent-text-color);
        }
        .thread-link:focus {
          color: var(--accent-text-color);
        }
      `}
    </style>
  </div>
);

Post.defaultProps = {
  isPreview: false,
  threadRemainedPosts: 0,
};

Post.propTypes = {
  isPreview: PropTypes.bool,
  indexInThread: PropTypes.number.isRequired,
  boardId: PropTypes.string.isRequired,
  threadRemainedPosts: PropTypes.number,
  post: postShape.isRequired,
};

export default Post;
