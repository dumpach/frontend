import React from 'react';
import PropTypes from 'prop-types';

const AttachmentPreview = ({ attachment, removeAttachment }) => (
  <div key={attachment.preview} className="attachment-preview-container">
    <img
      key={attachment.preview}
      className="attachment-preview"
      src={attachment.preview}
      alt={attachment.name}
    />
    <div className="attachment-preview-controls-container">
      <div className="attachment-preview-controls-file-name-container">
        <p className="attachment-preview-controls-file-name">{attachment.name}</p>
      </div>
      <i
        tabIndex="0"
        role="button"
        className="material-icons attachment-preview-controls-icon"
        onKeyPress={(event) => {
          event.stopPropagation();
          removeAttachment(attachment.name);
        }}
        onClick={(event) => {
          event.stopPropagation();
          removeAttachment(attachment.name);
        }}
      >
        highlight_off
      </i>
    </div>

    <style jsx>
      {`
        .attachment-preview-container {
          position: relative;
        }

        .attachment-preview {
          max-height: 100px;
          max-width: 100%;
          margin-bottom: 10px;
        }

        .attachment-preview-controls-container {
          display: flex;
          align-items: center;
          justify-content: space-between;
          position: absolute;
          height: 25px;
          bottom: 14px;
          width: 100%;
          background-color: rgba(0, 0, 0, 0.5);
        }

        .attachment-preview-controls-file-name-container {
          overflow-x: hidden;
          padding-left: 3px;
        }

        .attachment-preview-controls-file-name {
          color: var(--invert-text-color);
          font-size: 12px;
          margin: 0;
          white-space: nowrap;
        }

        .attachment-preview-controls-icon {
          transition: 0.1s;
          color: var(--invert-text-color);
        }
        .attachment-preview-controls-icon:hover {
          color: var(--accent-text-color);
        }
        .attachment-preview-controls-icon:focus {
          color: var(--accent-text-color);
        }
      `}
    </style>
  </div>
);

AttachmentPreview.propTypes = {
  attachment: PropTypes.shape({
    preview: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  removeAttachment: PropTypes.func.isRequired,
};

export default AttachmentPreview;
