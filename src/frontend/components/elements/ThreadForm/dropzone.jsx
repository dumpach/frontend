import React from 'react';
import Dropzone from 'react-dropzone';
import PropTypes from 'prop-types';

import AttachmentPreview from './attachment_preview';

const DropzoneContainer = ({
  setAttachments,
  setAttachmentsPreviews,
  attachmentsPreviews,
  removeAttachment,
}) => (
  <div>
    <Dropzone
      onDrop={(acceptedAttachments) => {
        setAttachmentsPreviews(
          acceptedAttachments.map((attachment) => ({
            preview: URL.createObjectURL(attachment),
            name: attachment.name,
          })),
        );
        setAttachments(acceptedAttachments);
      }}
    >
      {({ getRootProps, getInputProps }) => (
        <section>
          <div {...getRootProps()} className="dropzone">
            <input {...getInputProps()} />

            {attachmentsPreviews.length > 0 ? (
              <div className="attachments-preview-container">
                {attachmentsPreviews.map((attachmentPreview) => (
                  <AttachmentPreview
                    key={attachmentPreview.preview}
                    attachment={attachmentPreview}
                    removeAttachment={removeAttachment}
                  />
                ))}
              </div>
            ) : (
              <div className="dropzone-icon-container">
                <i className="material-icons dropzone-icon">attach_file</i>
              </div>
            )}
          </div>
        </section>
      )}
    </Dropzone>

    <style jsx>
      {`
        .dropzone {
          width: 100%;
          min-height: 150px;
          border: none;
          margin-bottom: 20px;
          cursor: pointer;
          outline: none;
        }

        .dropzone:hover .dropzone-icon {
          color: var(--accent-text-color);
          transition: 0.2s;
        }
        .dropzone:focus .dropzone-icon {
          color: var(--accent-text-color);
          transition: 0.2s;
        }

        .dropzone-icon-container {
          display: flex;
          align-items: center;
          justify-content: center;
          height: 150px;
        }

        .dropzone-icon {
          font-size: 4em;
        }

        .attachments-preview-container {
          height: 100%;
          display: flex;
          align-items: center;
          justify-content: space-between;
          flex-wrap: wrap;
        }
      `}
    </style>
  </div>
);

DropzoneContainer.propTypes = {
  setAttachments: PropTypes.func.isRequired,
  setAttachmentsPreviews: PropTypes.func.isRequired,
  attachmentsPreviews: PropTypes.arrayOf(
    PropTypes.shape({
      preview: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ).isRequired,
  removeAttachment: PropTypes.func.isRequired,
};

export default DropzoneContainer;
