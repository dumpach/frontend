import React from 'react';
import ReactDOM from 'react-dom';

import AttachmentPreview from './attachment_preview';

const testAttachmentPreview = {
  preview: 'blob:http://localhost:8080/9aa35b8b-16c7-4d06-b189-a1ca5a8a20e6',
  name: 'IMG_3476.JPG',
};

describe('Elements', () => {
  describe('AttachmentPreview', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(
        <AttachmentPreview
          attachment={testAttachmentPreview}
          removeAttachment={() => ({})}
        />,
        div,
      );
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
