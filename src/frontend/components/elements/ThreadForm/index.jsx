import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Dropzone from './dropzone';

const ThreadForm = ({
  onSubmit, isNewThread, isFetching, isOpen,
}) => {
  const [title, setTitle] = useState('');
  const [text, setText] = useState('');
  const [attachments, setAttachments] = useState([]);
  const [attachmentsPreviews, setAttachmentsPreviews] = useState([]);
  const [isSage, setIsSage] = useState(false);

  useEffect(
    () => () =>
      attachmentsPreviews.forEach((attachment) =>
        URL.revokeObjectURL(attachment.preview)),
    [],
  );

  const removeAttachment = (name) => {
    URL.revokeObjectURL(
      attachmentsPreviews.find(
        (attachmentPreview) => attachmentPreview.name === name,
      ).preview,
    );

    setAttachments(attachments.filter((attachment) => attachment.name !== name));
    setAttachmentsPreviews(
      attachmentsPreviews.filter(
        (attachmentPreview) => attachmentPreview.name !== name,
      ),
    );
  };

  const clearForm = () => {
    setTitle('');
    setText('');
    setAttachments([]);
    attachmentsPreviews.forEach((attachment) =>
      URL.revokeObjectURL(attachment.preview));
    setAttachmentsPreviews([]);
    setIsSage(false);
  };

  return (
    <form
      className={isOpen ? 'thread-form' : 'thread-form closed'}
      onSubmit={async (event) => {
        event.preventDefault();

        await onSubmit(
          {
            title,
            text,
            attachments,
            isSage,
          },
          clearForm,
        );
      }}
    >
      <input
        className="title-input form-input"
        placeholder="Title"
        type="text"
        value={title}
        onChange={(event) => setTitle(event.target.value)}
      />
      <textarea
        className="text-input form-input"
        placeholder="Text"
        type="text"
        value={text}
        onChange={(event) => setText(event.target.value)}
      />

      <Dropzone
        setAttachments={setAttachments}
        setAttachmentsPreviews={setAttachmentsPreviews}
        attachmentsPreviews={attachmentsPreviews}
        removeAttachment={removeAttachment}
      />

      <div className="form-button-container">
        <button className="form-button" disabled={isFetching} type="submit">
          Send
        </button>
      </div>

      <style jsx>
        {`
          .thread-form {
            margin-bottom: 20px;
          }

          .closed {
            display: none;
          }

          .form-input {
            display: block;
            min-width: 100%;
            min-height: 1.5em;
            border: none;
            background-color: var(--background-color);
            margin-bottom: 20px;
            border-bottom: 1px solid var(--main-text-color);
            outline: none;
            transition: 0.3s;
          }
          .form-input:focus {
            border-bottom: 1px solid var(--accent-text-color);
          }

          .text-input {
            max-width: 100%;
            min-height: 3em;
          }

          .form-button-container {
            text-align: center;
          }

          .form-button {
            width: 100%;
            background-color: var(--background-color);
            border: none;
            cursor: pointer;
            transition: 0.1s;
            outline: none;
          }
          .form-button:hover {
            color: var(--accent-text-color);
          }
          .form-button:focus {
            color: var(--accent-text-color);
          }
        `}
      </style>
    </form>
  );
};

ThreadForm.defaultProps = {
  isNewThread: false,
};

ThreadForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  isNewThread: PropTypes.bool,
  isFetching: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default ThreadForm;
