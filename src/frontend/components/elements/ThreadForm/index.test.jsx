import React from 'react';
import ReactDOM from 'react-dom';

import ThreadForm from '.';

describe('Elements', () => {
  describe('ThreadForm', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(
        <ThreadForm isOpen isFetching={false} onSubmit={() => ({})} />,
        div,
      );
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
