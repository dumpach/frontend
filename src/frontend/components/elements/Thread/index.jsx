import React from 'react';
import PropTypes from 'prop-types';

import Post from '../Post';

import { thread as threadShape } from '../../../shapes';

const Thread = ({ thread, isPreview }) => (
  <div className={isPreview ? 'thread thread-preview' : 'thread'}>
    {thread.posts.map((post, index) => (
      <Post
        post={post}
        boardId={thread.board_id}
        threadRemainedPosts={thread.remained_posts}
        key={post.id}
        indexInThread={index + 1}
        isPreview={isPreview}
      />
    ))}

    <style jsx>
      {`
        .thread-preview {    
          border-bottom: 1px solid var(--main-text-color);
          padding-bottom: 20px;
          margin-bottom: 20px;
        }

        .thread-preview:last-child {
          border-bottom: none;
          padding-bottom: 0;
        }
      `}
    </style>
  </div>
);

Thread.defaultProps = {
  isPreview: false,
};

Thread.propTypes = {
  thread: threadShape.isRequired,
  isPreview: PropTypes.bool,
};

export default Thread;
