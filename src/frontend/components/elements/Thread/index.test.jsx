import React from 'react';
import ReactDOM from 'react-dom';

import Thread from '.';

const testPost = {
  attachments: [],
  created_at: '2019-04-07T13:22:36.338Z',
  id: 1,
  is_sage: true,
  text: 'Test',
  thread_id: 1,
  replies: [
    {
      reply_id: 1,
      reply_thread_id: 1,
    },
  ],
  replies_on: [
    {
      post_id: 1,
      post_thread_id: 1,
    },
  ],
  title: 'Test',
  updated_at: '2019-04-07T13:22:36.338Z',
};

const testThread = {
  board_id: 'p',
  created_at: '2019-04-07T13:22:33.390Z',
  id: 1,
  posts: [testPost],
  remained_posts: 50,
  updated_at: '2019-04-07T13:22:33.390Z',
};

describe('Elements', () => {
  describe('Thread', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(
        <Thread key={testThread.id} thread={testThread} isPreview />,
        div,
      );
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
