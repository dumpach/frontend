import React from 'react';
import PropTypes from 'prop-types';

import { Link } from '../../../../renderer/routes';

const makePagesArray = (count) => {
  const pagesCount = Math.ceil(count / 10);
  const pagesArray = [];

  for (let i = 1; i <= pagesCount; i += 1) {
    pagesArray.push(i - 1);
  }

  return pagesArray;
};

const ThreadsPagination = ({
  boardId, count, pageId, updatePage,
}) => {
  const pages = makePagesArray(count);

  return (
    <div className="threads-pagination-container">
      {pages.map((page) => (
        <Link
          key={page}
          route={`/boards/${boardId}/${page}`}
          href={`/boards/${boardId}/${page}`}
          params={{ boardId, page }}
        >
          <a
            href={`/boards/${boardId}/${page}`}
            className={
              page === pageId
                ? 'threads-pagination-link active'
                : 'threads-pagination-link'
            }
            onClick={() => updatePage(page)}
          >
            {page}
          </a>
        </Link>
      ))}

      <style jsx>
        {`
          .threads-pagination-container {
            text-align: center;
            margin-bottom: 20px;
          }

          .threads-pagination-link {
            text-decoration: none;
            color: var(--main-text-color);
            margin-right: 10px;
            transition: 0.1s;
            outline: none;
          }
          .threads-pagination-link:hover {
            color: var(--accent-text-color);
          }
          .threads-pagination-link:focus {
            color: var(--accent-text-color);
          }
          .threads-pagination-link:last-child {
            margin-right: 0;
          }

          .active {
            color: var(--accent-text-color);
          }
        `}
      </style>
    </div>
  );
};

ThreadsPagination.propTypes = {
  boardId: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  pageId: PropTypes.number.isRequired,
  updatePage: PropTypes.func.isRequired,
};

export default ThreadsPagination;
