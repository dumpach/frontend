import React from 'react';
import ReactDOM from 'react-dom';

import ThreadsPagination from '.';

describe('Elements', () => {
  describe('ThreadsPagination', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(
        <ThreadsPagination
          boardId="b"
          count={50}
          pageId={2}
          updatePage={() => ({})}
        />,
        div,
      );
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
