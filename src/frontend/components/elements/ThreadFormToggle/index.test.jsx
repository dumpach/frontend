import React from 'react';
import ReactDOM from 'react-dom';

import ThreadFormToggle from '.';

describe('Elements', () => {
  describe('ThreadFormToggle', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(<ThreadFormToggle isOpen onToggle={() => ({})} />, div);
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
