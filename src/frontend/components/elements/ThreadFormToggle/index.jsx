import React from 'react';
import PropTypes from 'prop-types';

const ThreadFormToggle = ({ onToggle, isOpen }) => (
  <div className="thread-form-toggle-icon-container">
    <i
      tabIndex="0"
      role="button"
      className="material-icons thread-form-toggle-icon"
      onKeyPress={onToggle}
      onClick={onToggle}
    >
      {isOpen ? 'remove' : 'add'}
    </i>

    <style jsx>
      {`
        .thread-form-toggle-icon-container {
          text-align: center;
          margin-bottom: 20px;
        }

        .thread-form-toggle-icon {
          font-size: 3em;
          color: var(--main-text-color);
          cursor: pointer;
          transition: 0.1s;
          outline: none;
        }
        .thread-form-toggle-icon:hover {
          color: var(--accent-text-color);
        }
        .thread-form-toggle-icon:focus {
          color: var(--accent-text-color);
        }
      `}
    </style>
  </div>
);

ThreadFormToggle.propTypes = {
  onToggle: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default ThreadFormToggle;
