import React from 'react';
import PropTypes from 'prop-types';

import { Link, Router } from '../../../../renderer/routes';

const links = [
  {
    title: 'Home',
    path: '/',
  },
];

const Menu = ({ toggleOpen }) => (
  <div className="menu-container">
    <div className="close-menu-icon-container">
      <i
        tabIndex="0"
        role="button"
        className="material-icons close-menu-icon"
        onClick={toggleOpen}
        onKeyPress={toggleOpen}
      >
        close
      </i>
    </div>

    <div className="menu-list-container">
      <ul>
        {links.map((link) => (
          <li key={link.path}>
            <Link route={link.path} href={link.path}>
              <a
                href={link.path}
                className="board-link"
                onClick={async (event) => {
                  event.preventDefault();
                  await Router.pushRoute(link.path);
                  toggleOpen();
                }}
              >
                {link.title}
              </a>
            </Link>
          </li>
        ))}
      </ul>
    </div>

    <style jsx>
      {`
        .menu-container {
          position: fixed;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background-color: var(--background-color);
        }

        .close-menu-icon-container {
          position: fixed;
          top: 15px;
          left: 15px;
          outline: none;
        }

        .close-menu-icon {
          font-size: 1.5em;
          cursor: pointer;
          color: var(--main-text-color);
          outline: none;
        }
        .close-menu-icon:hover {
          color: var(--accent-text-color);
          transition: 0.1s;
        }
        .close-menu-icon:focus {
          color: var(--accent-text-color);
          transition: 0.1s;
        }

        .menu-list-container {
          display: flex;
          justify-content: center;
          align-items: center;
          height: 100%;
        }

        .board-link {
          color: inherit;
          display: block;
          text-decoration: none;
          font-size: 2.125em;
          transition: 0.1s;
        }

        .board-link:hover {
          color: var(--accent-text-color);
          font-size: 2.5em;
        }

        .board-link:focus {
          color: var(--accent-text-color);
          font-size: 2.5em;
        }
      `}
    </style>
  </div>
);

Menu.propTypes = {
  toggleOpen: PropTypes.func.isRequired,
};

export default Menu;
