import React from 'react';
import ReactDOM from 'react-dom';

import Menu from '.';

describe('Elements', () => {
  describe('Menu', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(
        <Menu toggleOpen={() => ({})} />,
        div,
      );
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
