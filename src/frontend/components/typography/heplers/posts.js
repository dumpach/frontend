export const focusPostAfterReplyLinkClick = (el) => {
  el.classList.add('post__focused');
  setTimeout(() => {
    el.classList.remove('post__focused');
  }, 3000);
};
