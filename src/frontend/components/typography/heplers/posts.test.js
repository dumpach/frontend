import { focusPostAfterReplyLinkClick } from './posts';

describe('Typography', () => {
  describe('helpers', () => {
    describe('posts', () => {
      describe('focusPostAfterReplyLinkClick', () => {
        it('renders without crashing', () => {
          const el = document.createElement('div');
          focusPostAfterReplyLinkClick(el);
          const { classList } = el;
          expect(classList.contains('post__focused')).toBeTruthy();
        });
      });
    });
  });
});
