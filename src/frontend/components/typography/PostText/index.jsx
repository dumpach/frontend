import React from 'react';
import PropTypes from 'prop-types';

const PostText = ({ text }) =>
  (text !== '' ? (
    <article className="post-text">
      {text}
      <style jsx>
        {`
          .post-text {
            font-weight: 300;
            font-size: 1.1em;
            text-align: justify;
            margin: 0;
            display: inline-block;
          }
        `}
      </style>
    </article>
  ) : (
    <br />
  ));

PostText.propTypes = {
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
};

export default PostText;
