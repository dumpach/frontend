import React from 'react';
import PropTypes from 'prop-types';

const PageTitle = ({ text }) => (
  <h1 className="page-title">
    {text}
    <style>
      {`
        .page-title {    
          text-align: center;
          font-size: 4em;
          font-weight: 300;
        }
      `}
    </style>
  </h1>
);

PageTitle.propTypes = {
  text: PropTypes.string.isRequired,
};

export default PageTitle;
