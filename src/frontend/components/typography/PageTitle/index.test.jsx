import React from 'react';
import ReactDOM from 'react-dom';

import PageTitle from '.';

describe('Typography', () => {
  describe('PageTitle', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(<PageTitle text="Test" />, div);
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
