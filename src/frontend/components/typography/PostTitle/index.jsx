import React from 'react';
import PropTypes from 'prop-types';

const PostText = ({ text }) => (
  <h3 className="post-title">
    {text}
    <style jsx>
      {`
        .post-title {
          font-weight: 400;
          text-align: justify;
          margin: 0 0 5px 0;
        }
      `}
    </style>
  </h3>
);

PostText.propTypes = {
  text: PropTypes.string.isRequired,
};

export default PostText;
