import React from 'react';
import ReactDOM from 'react-dom';

import PostText from '.';

describe('Typography', () => {
  describe('PageTitle', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(<PostText text="Test" />, div);
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
