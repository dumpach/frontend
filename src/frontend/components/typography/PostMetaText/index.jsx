import React from 'react';
import PropTypes from 'prop-types';

const PostMetaText = ({ text }) => (
  <p className="post-meta-text">
    {text}
    <style jsx>
      {`
        .post-meta-text {
          display: inline-block;
          font-size: 0.8em;
          margin: 0 10px 5px 0;
        }
      `}
    </style>
  </p>
);

PostMetaText.propTypes = {
  text: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.element])
    .isRequired,
};

export default PostMetaText;
