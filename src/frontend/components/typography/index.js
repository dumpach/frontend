import PageTitle from './PageTitle';
import PostTitle from './PostTitle';
import PostText from './PostText';
import PostMetaText from './PostMetaText';
import PostReplyLink from './PostReplyLink';
import PostReplyOnLink from './PostReplyOnLink';

export {
  PageTitle,
  PostTitle,
  PostText,
  PostMetaText,
  PostReplyLink,
  PostReplyOnLink,
};
