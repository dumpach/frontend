import React from 'react';
import PropTypes from 'prop-types';

import { posts } from '../heplers';
import { Link } from '../../../../renderer/routes';

const PostReplyLink = ({ boardId, reply }) => (
  <div className="post-reply-link-container">
    <Link
      route={`/boards/${boardId}/threads/${reply.reply_thread_id}#${reply.reply_id}`}
      href={`/boards/${boardId}/threads/${reply.reply_thread_id}#${reply.reply_id}`}
      params={{ boardId, threadId: reply.reply_thread_id }}
    >
      <a
        href={`/boards/${boardId}/threads/${reply.reply_thread_id}#${
          reply.reply_id
        }`}
        className="post-reply-link"
        onClick={() => {
          const el = document.getElementById(reply.reply_id);
          if (el) {
            posts.focusPostAfterReplyLinkClick(el);
          }
        }}
        // onMouseEnter={() => {
        //   const el = document.getElementById(reply.reply_id);
        //   if (el) {
        //     el.classList.add('post-focused');
        //   }
        // }}
        // onMouseLeave={() => {
        //   const el = document.getElementById(reply.reply_id);
        //   if (el) {
        //     el.classList.remove('post-focused');
        //   }
        // }}
      >
        {`>>${reply.reply_id}`}
      </a>
    </Link>

    <style jsx>
      {`
        .post-reply-link-container {
          display: inline-block;
          margin: 0 5px 0 0;
        }

        .post-reply-link {
          font-size: 0.8em;
          color: var(--accent-text-color);
          transition: 0.1s;
        }
      `}
    </style>
  </div>
);

PostReplyLink.propTypes = {
  boardId: PropTypes.string.isRequired,
  reply: PropTypes.shape({
    reply_id: PropTypes.number.isRequired,
    reply_thread_id: PropTypes.number.isRequired,
  }).isRequired,
};

export default PostReplyLink;
