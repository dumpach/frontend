import React from 'react';
import ReactDOM from 'react-dom';

import PostReplyLink from '.';

const testReply = {
  reply_id: 1,
  reply_thread_id: 1,
};

describe('Typography', () => {
  describe('PostReplyOnLink', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(<PostReplyLink boardId="b" reply={testReply} />, div);
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
