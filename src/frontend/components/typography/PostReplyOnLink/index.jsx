import React from 'react';
import PropTypes from 'prop-types';

import { posts } from '../heplers';
import { Link } from '../../../../renderer/routes';

const PostReplyOnLink = ({ boardId, replyOn }) => (
  <div className="post-reply-on-link-container">
    <Link
      route={`/boards/${boardId}/threads/${replyOn.post_thread_id}#${
        replyOn.post_id
      }`}
      href={`/boards/${boardId}/threads/${replyOn.post_thread_id}#${
        replyOn.post_id
      }`}
      params={{ boardId, threadId: replyOn.post_thread_id }}
    >
      <a
        href={`/boards/${boardId}/threads/${replyOn.post_thread_id}#${
          replyOn.post_id
        }`}
        className="post-reply-on-link"
        onClick={() => {
          const el = document.getElementById(replyOn.post_id);
          if (el) {
            posts.focusPostAfterReplyLinkClick(el);
          }
        }}
        // onMouseEnter={() => {
        //   const el = document.getElementById(replyOn.post_id);
        //   if (el) {
        //     el.classList.add('post-focused');
        //   }
        // }}
        // onMouseLeave={() => {
        //   const el = document.getElementById(replyOn.post_id);
        //   if (el) {
        //     el.classList.remove('post-focused');
        //   }
        // }}
      >
        {`>>${replyOn.post_id}`}
      </a>
    </Link>

    <style jsx>
      {`
        .post-reply-on-link-container {
          display: inline-block;
        }

        .post-reply-on-link {
          font-weight: 300;
          font-size: 1em;
          color: var(--accent-text-color);
          transition: 0.1s;
        }
      `}
    </style>
  </div>
);

PostReplyOnLink.propTypes = {
  boardId: PropTypes.string.isRequired,
  replyOn: PropTypes.shape({
    post_id: PropTypes.number.isRequired,
    post_thread_id: PropTypes.number.isRequired,
  }).isRequired,
};

export default PostReplyOnLink;
