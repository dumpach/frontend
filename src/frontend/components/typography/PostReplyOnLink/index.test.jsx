import React from 'react';
import ReactDOM from 'react-dom';

import PostReplyOnLink from '.';

const testReplyOn = {
  post_id: 1,
  post_thread_id: 1,
};

describe('Typography', () => {
  describe('PostReplyOnLink', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');

      ReactDOM.render(<PostReplyOnLink boardId="b" replyOn={testReplyOn} />, div);
      ReactDOM.unmountComponentAtNode(div);
    });
  });
});
