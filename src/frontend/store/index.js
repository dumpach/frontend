import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { isServer, env } from '../config/app';

import reducers from './reducers';

export const initializeStore = (initialState = {}) => {
  const middlewares = [thunk];

  if (!isServer && env === 'development') {
    middlewares.push(createLogger());
  }

  return createStore(
    reducers,
    initialState,
    applyMiddleware(...middlewares),
  );
};
