import types from '../types';
import {
  IS_EMPTY,
  IS_FETCHING,
  IS_FETCHED,
  IS_ERROR,
  IS_NOT_FOUND,
} from '../status';

const initialState = {
  status: IS_EMPTY,
  data: {},
};

const threads = (state = initialState, action) => {
  switch (action.type) {
    case types.thread.GET_THREAD:
      return { status: IS_FETCHING, data: state.data };
    case types.thread.GET_THREAD_SUCCESS:
      return {
        status: IS_FETCHED,
        data: action.data,
      };
    case types.thread.GET_THREAD_ERROR:
      return {
        status: IS_ERROR,
        data: {},
      };
    case types.thread.GET_THREAD_NOT_FOUND:
      return {
        status: IS_NOT_FOUND,
        data: {},
      };
    case types.thread.CREATE_THREAD:
      return {
        status: IS_FETCHING,
        data: state.data,
      };
    case types.thread.CREATE_THREAD_SUCCESS:
      return {
        status: IS_FETCHED,
        data: action.data,
      };
    case types.thread.CREATE_THREAD_ERROR:
      return {
        status: IS_ERROR,
        data: {},
      };
    case types.thread.UPDATE_THREAD_SUCCESS:
      return {
        status: IS_FETCHED,
        data: action.data,
      };
    case types.thread.UPDATE_THREAD_ERROR:
      return {
        status: IS_ERROR,
        data: state.data,
      };
    default:
      return state;
  }
};

export default threads;
