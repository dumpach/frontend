import types from '../types';
import {
  IS_EMPTY, IS_FETCHING, IS_FETCHED, IS_ERROR,
} from '../status';

const initialState = {
  status: IS_EMPTY,
  data: [],
};

const boards = (state = initialState, action) => {
  switch (action.type) {
    case types.boards.GET_BOARDS:
      return {
        status: IS_FETCHING,
        data: [],
      };
    case types.boards.GET_BOARDS_SUCCESS:
      return {
        status: IS_FETCHED,
        data: action.data,
      };
    case types.boards.GET_BOARDS_FAIL:
      return {
        status: IS_ERROR,
        data: [],
      };
    default:
      return state;
  }
};

export default boards;
