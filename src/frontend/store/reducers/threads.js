import types from '../types';
import {
  IS_EMPTY,
  IS_FETCHING,
  IS_FETCHED,
  IS_ERROR,
  IS_NOT_FOUND,
} from '../status';

const initialState = {
  status: IS_EMPTY,
  data: [],
  count: 0,
  isLastPage: true,
};

const threads = (state = initialState, action) => {
  switch (action.type) {
    case types.threads.GET_THREADS:
      return {
        ...state,
        status: IS_FETCHING,
        data: [],
      };
    case types.threads.GET_THREADS_SUCCESS:
      return {
        status: IS_FETCHED,
        data: action.data.data,
        count: action.data.count,
        isLastPage: action.data.is_last_page,
      };
    case types.threads.GET_THREADS_ERROR:
      return {
        status: IS_ERROR,
        data: [],
        count: 0,
        isLastPage: true,
      };
    case types.threads.GET_THREADS_NOT_FOUND:
      return {
        status: IS_NOT_FOUND,
        data: [],
        count: 0,
        isLastPage: true,
      };
    default:
      return state;
  }
};

export default threads;
