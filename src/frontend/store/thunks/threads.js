import axios from 'axios';

import config from '../../config';
import types from '../types';

export const getThreads = (boardId, pageId = 0) => async (dispatch) => {
  try {
    dispatch({
      type: types.threads.GET_THREADS,
    });

    const response = await axios.get(
      `${
        config.app.api.endpoint
      }/boards/${boardId}/threads?offset=${pageId * 10}`,
    );

    dispatch({
      type: types.threads.GET_THREADS_SUCCESS,
      data: response.data,
    });
  } catch (err) {
    if (err.response.status === 404) {
      dispatch({
        type: types.threads.GET_THREADS_NOT_FOUND,
      });
    } else {
      dispatch({
        type: types.threads.GET_THREADS_ERROR,
      });
    }
  }

  return {};
};
