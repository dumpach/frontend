import axios from 'axios';

import { Router } from '../../../renderer/routes';
import config from '../../config';
import types from '../types';

const parseAnswersFromText = (text) => {
  const stringIds = text.match(/>>\d+/gm) || [];
  const ids = stringIds.map((stringId) => {
    const stringsArr = stringId.split('>>');
    const id = parseInt(stringsArr[1], 10);
    return id;
  });
  return ids;
};

export const getThread = (boardId, threadId) => async (dispatch) => {
  try {
    dispatch({
      type: types.thread.GET_THREAD,
    });

    const response = await axios.get(
      `${config.app.api.endpoint}/boards/${boardId}/threads/${threadId}`,
    );

    dispatch({
      type: types.thread.GET_THREAD_SUCCESS,
      data: response.data.data,
    });
  } catch (err) {
    if (err.response.status === 404) {
      dispatch({
        type: types.thread.GET_THREAD_NOT_FOUND,
      });
    } else {
      dispatch({
        type: types.thread.GET_THREAD_ERROR,
      });
    }
  }

  return {};
};

export const createThread = (boardId, data, clearFormCallback) => async (
  dispatch,
) => {
  try {
    dispatch({
      type: types.thread.CREATE_THREAD,
    });

    const repliesOn = parseAnswersFromText(data.text);

    const formData = new FormData();
    formData.append('title', data.title);
    formData.append('text', data.text);
    formData.append('is_sage', data.isSage);
    formData.append('replies_on', repliesOn);
    data.attachments.forEach((attachment, index) =>
      formData.append(`file_${index}`, attachment));

    const response = await axios.post(
      `${config.app.api.endpoint}/boards/${boardId}/threads`,
      formData,
    );

    clearFormCallback();

    dispatch({
      type: types.thread.CREATE_THREAD_SUCCESS,
      data: response.data.data,
    });

    Router.pushRoute(`/boards/${boardId}/threads/${response.data.data.id}`);
  } catch (err) {
    dispatch({
      type: types.thread.CREATE_THREAD_ERROR,
    });

    // dispatch({
    //   type: types.notification.SHOW_NOTIFICATION,
    //   data: { message: err.response.data.error.message },
    // });
  }

  return {};
};

export const updateThread = (boardId, threadId, data, clearFormCallback) => async (
  dispatch,
) => {
  try {
    dispatch({
      type: types.thread.UPDATE_THREAD,
    });

    const repliesOn = parseAnswersFromText(data.text);

    const formData = new FormData();
    formData.append('title', data.title);
    formData.append('text', data.text);
    formData.append('is_sage', data.isSage);
    formData.append('replies_on', repliesOn);
    data.attachments.forEach((attachment, index) =>
      formData.append(`file_${index}`, attachment));

    const response = await axios.post(
      `${config.app.api.endpoint}/boards/${boardId}/threads/${threadId}`,
      formData,
    );

    clearFormCallback();

    dispatch({
      type: types.thread.UPDATE_THREAD_SUCCESS,
      data: response.data.data,
    });
  } catch (err) {
    dispatch({
      type: types.thread.UPDATE_THREAD_ERROR,
    });

    // dispatch({
    //   type: types.notification.SHOW_NOTIFICATION,
    //   data: { message: err.response.data.error.message },
    // });
  }

  return {};
};
