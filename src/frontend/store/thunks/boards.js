import axios from 'axios';

import config from '../../config';
import types from '../types';

export const getBoards = () => async (dispatch) => {
  try {
    dispatch({
      type: types.boards.GET_BOARDS,
      data: {},
    });

    const response = await axios.get(`${config.app.api.endpoint}/boards`);

    dispatch({
      type: types.boards.GET_BOARDS_SUCCESS,
      data: response.data.data,
    });

    return {};
  } catch (err) {
    // TODO: Add error dispatching
    console.log(err);
    dispatch({
      type: types.boards.GET_BOARDS_FAIL,
      data: {},
    });
  }

  return {};
};
