export const IS_EMPTY = 'IS_EMPTY';
export const IS_FETCHING = 'IS_FETCHING';
export const IS_FETCHED = 'IS_FETCHED';
export const IS_NOT_FOUND = 'IS_NOT_FOUND';
export const IS_ERROR = 'IS_ERROR';
