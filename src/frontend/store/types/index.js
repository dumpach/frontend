import boards from './boards';
import notification from './notification';
import settings from './settings';
import thread from './thread';
import threads from './threads';

const types = {
  boards,
  notification,
  settings,
  thread,
  threads,
};

export default types;
