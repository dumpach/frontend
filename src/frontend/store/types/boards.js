const GET_BOARDS = '@@boards/GET_BOARDS';
const GET_BOARDS_SUCCESS = '@@boards/GET_BOARDS_SUCCESS';
const GET_BOARDS_FAIL = '@@boards/GET_BOARDS_FAIL';

const boards = {
  GET_BOARDS,
  GET_BOARDS_SUCCESS,
  GET_BOARDS_FAIL,
};

export default boards;
