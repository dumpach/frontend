const express = require('express');
const next = require('next');
const path = require('path');
const routes = require('./routes');

const port = parseInt(process.env.PORT, 10) || 8080;
const { NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';
const app = next({
  dev,
  dir: path.join(__dirname, '../frontend'),
});
const handler = routes.getRequestHandler(app);
const server = express();

app.prepare().then(() => {
  if (dev) {
    /* eslint-disable global-require */
    /* eslint-disable-next-line import/no-extraneous-dependencies */
    const proxy = require('http-proxy-middleware');
    /* eslint-enable global-require */
    server.use('/api', proxy({ target: 'http://backend:3000' }));
  }

  server.all('*', (req, res) => handler(req, res));

  server.listen(port, (err) => {
    if (err) {
      throw err;
    }
    console.log('renderer - online');

    if (NODE_ENV === 'test') {
      setTimeout(() => process.exit(0), 5000);
    }
  });
});
