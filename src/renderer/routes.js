const nextRoutes = require('next-routes');

const routes = nextRoutes();

routes.add('board', '/boards/:boardId/:pageId?');
routes.add('thread', '/boards/:boardId/threads/:threadId');

module.exports = routes;
