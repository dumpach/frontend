FROM node:10-alpine

WORKDIR /usr/src/app

COPY package.json ./
COPY package-lock.json ./

# ARG CACHEBUST=1

RUN npm install

COPY next.config.js ./
COPY src ./

EXPOSE 8080

CMD ["npm", "run", "dev"]