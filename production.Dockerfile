FROM node:10-alpine

WORKDIR /usr/src/app

COPY package.json ./
COPY package-lock.json ./

# ARG CACHEBUST=1

RUN npm install --only=production

COPY next.config.js ./
COPY src/ ./src/

RUN npm run build

EXPOSE 8080

CMD ["npm", "start"]