// Express requirements
import path from 'path';
import fs from 'fs';

// React requirements
import React from 'react';
import { renderToString } from 'react-dom/server';
import Helmet from 'react-helmet';
import { Provider } from 'react-redux';
/* eslint-disable-next-line import/no-extraneous-dependencies */
import { StaticRouter } from 'react-router';
import { Frontload, frontloadServerRender } from 'react-frontload';
import Loadable from 'react-loadable';
import { SheetsRegistry } from 'jss';
import JssProvider from 'react-jss/lib/JssProvider';
import { MuiThemeProvider, createGenerateClassName } from '@material-ui/core/styles';

import createStore from '../src/store';
import { chooseTheme } from '../src/themes';
import App from '../src/app/app';
import manifest from '../build/asset-manifest.json';

export default (req, res) => {
  const injectHTML = (data, {
    html, title, meta, body, scripts, state, css,
  }) => {
    /* eslint-disable no-param-reassign */
    data = data.replace('<html>', `<html ${html}>`);
    data = data.replace(/<title>.*?<\/title>/g, title);
    data = data.replace('</head>', `${meta}</head>`);
    data = data.replace(
      '<div id="root"></div>',
      `<div id="root">${body}</div><script>window.__PRELOADED_STATE__ = ${state}</script>`,
    );
    data = data.replace(
      '<style id="jss-server-side"></style>',
      `<style id="jss-server-side">${css}</style>`,
    );
    data = data.replace('</body>', `${scripts.join('')}</body>`);
    /* eslint-enable no-param-reassign */

    return data;
  };

  /* eslint-disable-next-line consistent-return */
  fs.readFile(path.resolve(__dirname, '../build/index.html'), 'utf8', (err, htmlData) => {
    if (err) {
      console.error('Read error', err);

      return res.status(404).end();
    }

    const { store } = createStore(req.url, req.cookies);
    const state = store.getState();
    const theme = chooseTheme(state.settings.stylingTheme);

    const context = {};
    const modules = [];

    const sheetsRegistry = new SheetsRegistry();

    const sheetsManager = new Map();

    const generateClassName = createGenerateClassName();

    frontloadServerRender(() =>
      renderToString(
        <Loadable.Capture report={(m) => modules.push(m)}>
          <Provider store={store}>
            <JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
              <MuiThemeProvider theme={theme} sheetsManager={sheetsManager}>
                <StaticRouter location={req.url} context={context}>
                  <Frontload isServer>
                    <App />
                  </Frontload>
                </StaticRouter>
              </MuiThemeProvider>
            </JssProvider>
          </Provider>
        </Loadable.Capture>,
      )).then((routeMarkup) => {
      if (context.url) {
        res.writeHead(302, {
          Location: context.url,
        });

        res.end();
      } else {
        const extractAssets = (assets, chunks) =>
          Object.keys(assets)
            .filter((asset) => chunks.indexOf(asset.replace('.js', '')) > -1)
            .map((k) => assets[k]);

        const extraChunks = extractAssets(manifest, modules).map(
          (c) => `<script type="text/javascript" src="/${c.replace(/^\//, '')}"></script>`,
        );

        const helmet = Helmet.renderStatic();

        const css = sheetsRegistry.toString();

        const html = injectHTML(htmlData, {
          html: helmet.htmlAttributes.toString(),
          title: helmet.title.toString(),
          meta: helmet.meta.toString(),
          body: routeMarkup,
          scripts: extraChunks,
          state: JSON.stringify(store.getState()).replace(/</g, '\\u003c'),
          css,
        });

        res.send(html);
      }
    });
  });
};
