
/* eslint-disable-next-line import/no-extraneous-dependencies */
const proxy = require('http-proxy-middleware');

module.exports = (app) => {
  app.use(proxy('/api', { target: 'http://backend:3000' }));
};
