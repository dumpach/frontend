import { combineReducers } from 'redux';
import boards from './boards';
import notification from './notification';
import settings from './settings';
import thread from './thread';
import threads from './threads';

const reducers = (cookies) => combineReducers({
  boards,
  notification,
  settings: settings(cookies),
  thread,
  threads,
});

export default reducers;
