import types from '../types';

const initialState = {
  isOpen: false,
  message: '',
};

const notification = (state = initialState, action) => {
  switch (action.type) {
    case types.notification.SHOW_NOTIFICATION:
      return {
        isOpen: true,
        message: action.data.message,
      };
    case types.notification.HIDE_NOTIFICATION:
      return {
        isOpen: false,
        message: state.message,
      };
    default:
      return state;
  }
};

export default notification;
