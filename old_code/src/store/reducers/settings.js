import { themes } from '../../themes';
import types from '../types';

const settings = (cookies) => {
  const stylingThemes = Object.keys(themes);
  const stylingTheme = stylingThemes.includes(cookies.theme) ? cookies.theme : 'light';

  const initialState = {
    paginationThreadsPerPage: parseInt(cookies.pagination, 10) || 10,
    stylingTheme,
    stylingThemes,
  };

  return (state = initialState, action) => {
    switch (action.type) {
      case types.settings.UPDATE_THEME:
        return { ...state, stylingTheme: action.data.theme };
      case types.settings.UPDATE_PAGINATION:
        return { ...state, paginationThreadsPerPage: action.data.threadsPerPage };
      default:
        return state;
    }
  };
};

export default settings;
