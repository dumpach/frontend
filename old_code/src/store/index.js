import { createStore, applyMiddleware, compose } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
/* eslint-disable-next-line import/no-extraneous-dependencies */
import { createBrowserHistory, createMemoryHistory } from 'history';

import reducers from './reducers';

// A nice helper to tell us if we're on the server
export const isServer = !(
  typeof window !== 'undefined'
  && window.document
  && window.document.createElement
);

export default (url = '/', cookies) => {
  // Create a history depending on the environment
  const history = isServer
    ? createMemoryHistory({
      initialEntries: [url],
    })
    : createBrowserHistory();

  const enhancers = [];

  // Dev tools are helpful
  if (process.env.NODE_ENV === 'development' && !isServer) {
    const { devToolsExtension } = window;

    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension());
    }
  }

  const middlewares = [thunk, routerMiddleware(history)];
  if (process.env.NODE_ENV === 'development') {
    middlewares.push(createLogger());
  }

  const composedEnhancers = compose(
    applyMiddleware(...middlewares),
    ...enhancers,
  );

  // Do we have preloaded state available? Great, save it.
  /* eslint-disable no-underscore-dangle */
  const initialState = !isServer ? window.__PRELOADED_STATE__ : {};

  // Delete it once we have it stored in a variable
  if (!isServer) {
    delete window.__PRELOADED_STATE__;
  }
  /* eslint-enable no-underscore-dangle */

  // Create the store
  const store = createStore(
    connectRouter(history)(reducers(cookies)),
    initialState,
    composedEnhancers,
  );

  // sagaMiddleware.run(sagas);

  return {
    store,
    history,
  };
};
