const UPDATE_THEME = '@@settings/UPDATE_THEME';
const UPDATE_PAGINATION = '@@settings/UPDATE_PAGINATION';

const settings = {
  UPDATE_THEME,
  UPDATE_PAGINATION,
};

export default settings;
