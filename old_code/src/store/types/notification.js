const SHOW_NOTIFICATION = '@@notification/SHOW_NOTIFICATION';
const HIDE_NOTIFICATION = '@@notification/HIDE_NOTIFICATION';

const notification = {
  SHOW_NOTIFICATION,
  HIDE_NOTIFICATION,
};

export default notification;
