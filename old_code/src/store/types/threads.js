const GET_THREADS = '@@threads/GET_THREADS';
const GET_THREADS_SUCCESS = '@@threads/GET_THREADS_SUCCESS';
const GET_THREADS_FAIL = '@@threads/GET_THREADS_FAIL';

const threads = {
  GET_THREADS,
  GET_THREADS_SUCCESS,
  GET_THREADS_FAIL,
};

export default threads;
