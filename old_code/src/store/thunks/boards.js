import axios from 'axios';

import config from '../../config';
import types from '../types';

export const getBoards = () => (dispatch) => new Promise(async (resolve) => {
  try {
    dispatch({
      type: types.boards.GET_BOARDS,
      data: {},
    });

    const response = await axios.get(`${config.app.api.endpoint}/boards`);

    if (response.status === 200) {
      dispatch({
        type: types.boards.GET_BOARDS_SUCCESS,
        data: response.data.data,
      });
    }

    resolve(response.data.data);
  } catch (err) {
    // TODO: Add error dispatching
    console.log(err);
    dispatch({
      type: types.boards.GET_BOARDS_FAIL,
      data: {},
    });
  }
});
