import axios from 'axios';

import config from '../../config';
import types from '../types';

export const getThreads = (boardId, threadsPerPage, offset) => (dispatch) => new Promise(
  async (resolve) => {
    try {
      dispatch({
        type: types.threads.GET_THREADS,
        data: {},
      });

      const response = await axios.get(
        `${
          config.app.api.endpoint
        }/boards/${boardId}/threads?limit=${threadsPerPage}&offset=${offset}`,
      );

      if (response.status === 200) {
        dispatch({
          type: types.threads.GET_THREADS_SUCCESS,
          data: response.data,
        });
      }

      resolve(response.data);
    } catch (err) {
      // TODO: Add error dispatching
      console.log(err);
      dispatch({
        type: types.threads.GET_THREADS_FAIL,
        data: {},
      });
    }
  },
);
