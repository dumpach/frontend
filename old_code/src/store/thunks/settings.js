import types from '../types';

export const updateTheme = (theme) => (dispatch) =>
  dispatch({
    type: types.settings.UPDATE_THEME,
    data: { theme },
  });

export const updatePagination = (threadsPerPage) => (dispatch) =>
  dispatch({
    type: types.settings.UPDATE_PAGINATION,
    data: { threadsPerPage },
  });
