import axios from 'axios';
import { push } from 'connected-react-router';

import config from '../../config';
import types from '../types';

export const getThread = (boardId, threadId) => (dispatch) =>
  new Promise(async (resolve) => {
    try {
      dispatch({
        type: types.thread.GET_THREAD,
      });

      const response = await axios.get(
        `${config.app.api.endpoint}/boards/${boardId}/threads/${threadId}`,
      );

      if (response.status === 200) {
        dispatch({
          type: types.thread.GET_THREAD_SUCCESS,
          data: response.data.data,
        });

        resolve(response.data.data);
      }
    } catch (err) {
      // TODO: Add error view dispatch
      console.log(err);
      dispatch({
        type: types.thread.CREATE_THREAD_FAIL,
      });
    }
  });

export const createThread = (boardId, data, clearForm) => (dispatch) =>
  new Promise(async (resolve) => {
    try {
      dispatch({
        type: types.thread.CREATE_THREAD,
      });

      const formData = new FormData();
      formData.append('title', data.title);
      formData.append('text', data.text);
      formData.append('is_sage', data.isSage);
      data.attachments.forEach((attachment, index) => formData.append(`file_${index}`, attachment));

      const response = await axios.post(
        `${config.app.api.endpoint}/boards/${boardId}/threads`,
        formData,
      );

      if (response.status === 201) {
        clearForm();

        dispatch({
          type: types.thread.CREATE_THREAD_SUCCESS,
          data: response.data.data,
        });

        dispatch(push(`/${boardId}/threads/${response.data.data.id}`));

        resolve(response.data.data);
      }
    } catch (err) {
      dispatch({
        type: types.thread.CREATE_THREAD_FAIL,
      });

      dispatch({
        type: types.notification.SHOW_NOTIFICATION,
        data: { message: err.response.data.error.message },
      });
    }
  });

export const updateThread = (boardId, threadId, data, clearForm) => (dispatch) =>
  new Promise(async (resolve) => {
    try {
      dispatch({
        type: types.thread.UPDATE_THREAD,
      });

      const formData = new FormData();
      formData.append('title', data.title);
      formData.append('text', data.text);
      formData.append('is_sage', data.isSage);
      data.attachments.forEach((attachment, index) => formData.append(`file_${index}`, attachment));

      const response = await axios.post(
        `${config.app.api.endpoint}/boards/${boardId}/threads/${threadId}`,
        formData,
      );

      if (response.status === 200) {
        clearForm();

        dispatch({
          type: types.thread.UPDATE_THREAD_SUCCESS,
          data: response.data.data,
        });

        resolve(response.data.data);
      }
    } catch (err) {
      dispatch({
        type: types.thread.UPDATE_THREAD_FAIL,
      });

      dispatch({
        type: types.notification.SHOW_NOTIFICATION,
        data: { message: err.response.data.error.message },
      });
    }
  });
