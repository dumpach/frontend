import { isServer } from '../store';

const { NODE_ENV, API_HOST, API_PORT } = process.env;

const development = {
  api: {
    endpoint: '/api/v1',
  },
};

const test = {
  api: {
    endpoint: '/api/v1',
  },
};

const production = {
  api: {
    endpoint: isServer ? `http://${API_HOST}:${API_PORT}/api/v1` : '/api/v1',
  },
};

const config = {
  development,
  test,
  production,
};

export default config[NODE_ENV];
