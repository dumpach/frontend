import React from 'react';
import { render, hydrate } from 'react-dom';
import { Provider } from 'react-redux';
import Loadable from 'react-loadable';
import { Frontload } from 'react-frontload';
import { ConnectedRouter } from 'connected-react-router';
import JssProvider from 'react-jss/lib/JssProvider';

import { MuiThemeProvider, createGenerateClassName } from '@material-ui/core/styles';

import createStore from './store';

import { chooseTheme } from './themes';

import { cookies } from './app/helpers';

import App from './app/app';

const { store, history } = createStore(null, {
  theme: cookies.getCookie('theme'),
  pagination: cookies.getCookie('pagination'),
});

class Main extends React.Component {
  componentDidMount() {
    const jssStyles = document.getElementById('jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    return <App />;
  }
}

const generateClassName = createGenerateClassName();

const state = store.getState();
const theme = chooseTheme(state.settings.stylingTheme);

const Application = (
  <Provider store={store}>
    <JssProvider generateClassName={generateClassName}>
      <MuiThemeProvider theme={theme}>
        <ConnectedRouter history={history}>
          <Frontload noServerRender={process.env.NODE_ENV !== 'production'}>
            <Main />
          </Frontload>
        </ConnectedRouter>
      </MuiThemeProvider>
    </JssProvider>
  </Provider>
);

const root = document.querySelector('#root');

if (root.hasChildNodes() === true) {
  Loadable.preloadReady().then(() => {
    hydrate(Application, root);
  });
} else {
  render(Application, root);
}
