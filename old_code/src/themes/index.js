import light from './light';
import dark from './dark';

export const themes = {
  light,
  dark,
};

export const chooseTheme = (theme = 'light') => {
  let chosenTheme = themes[theme];

  if (chosenTheme === undefined) {
    chosenTheme = light;
  }

  return chosenTheme;
};
