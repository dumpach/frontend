import PropTypes from 'prop-types';

import post from './post';

const thread = PropTypes.shape({
  id: PropTypes.number.isRequired,
  board_id: PropTypes.string.isRequired,
  created_at: PropTypes.string.isRequired,
  updated_at: PropTypes.string.isRequired,
  remained_posts: PropTypes.number,
  posts: PropTypes.arrayOf(post).isRequired,
});

export default thread;
