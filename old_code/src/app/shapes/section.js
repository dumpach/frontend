import PropTypes from 'prop-types';

import board from './board';

const section = PropTypes.shape({
  created_at: PropTypes.string.isRequired,
  updated_at: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  boards: PropTypes.arrayOf(board).isRequired,
});

export default section;
