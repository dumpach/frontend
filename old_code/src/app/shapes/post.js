import PropTypes from 'prop-types';

import attachment from './attachment';

const post = PropTypes.shape({
  id: PropTypes.number.isRequired,
  thread_id: PropTypes.number.isRequired,
  created_at: PropTypes.string.isRequired,
  updated_at: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  is_sage: PropTypes.bool.isRequired,
  attachments: PropTypes.arrayOf(attachment).isRequired,
});

export default post;
