import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

import Post from '../post';

import { thread as threadShape } from '../../shapes';

const styles = (theme) => ({
  threadPreviewContainer: {
    borderBottom: `1px solid ${theme.palette.text.hint}`,
    paddingBottom: 20,
    [theme.breakpoints.up('xs')]: {
      margin: '0 10px',
    },
    [theme.breakpoints.up('sm')]: {
      margin: '0 10%',
    },
    [theme.breakpoints.up('md')]: {
      margin: '0 20%',
    },
    [theme.breakpoints.up('lg')]: {
      margin: '0 20%',
    },
    [theme.breakpoints.up('xl')]: {
      margin: '0 30%',
    },
  },
});

const ThreadPreview = ({ thread, classes, isLastThreadInList }) => (
  <div
    className={classes.threadPreviewContainer}
    style={{
      borderBottom: isLastThreadInList ? 'none' : null,
      marginBottom: isLastThreadInList ? 'none' : 30,
    }}
  >
    {thread.posts.map((post, index) => (
      <Post
        post={post}
        boardId={thread.board_id}
        threadRemainedPosts={thread.remained_posts}
        key={post.id}
        indexInThread={index + 1}
        isPreview
      />
    ))}
  </div>
);

ThreadPreview.propTypes = {
  classes: PropTypes.shape({
    threadPreviewContainer: PropTypes.string.isRequired,
  }).isRequired,
  thread: threadShape.isRequired,
  isLastThreadInList: PropTypes.bool.isRequired,
};

export default withStyles(styles)(ThreadPreview);
