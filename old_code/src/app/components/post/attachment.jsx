import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';

import { attachment as attachmentShape } from '../../shapes';

const styles = () => ({
  attachmentContainer: {
    maxWidth: 150,
    maxHeight: 150,
  },
});

const PostAttachments = ({
  attachment, boardId, threadId, classes,
}) => (
  <Link
    to={`/uploads/source/${boardId}/${threadId}/${attachment.uuid}-${attachment.name}`}
    target="blank"
  >
    <img
      className={classes.attachmentContainer}
      alt={attachment.name}
      src={`/uploads/thumb/${boardId}/${threadId}/${attachment.uuid}-${attachment.name}`}
    />
  </Link>
);

PostAttachments.propTypes = {
  classes: PropTypes.shape({
    attachmentContainer: PropTypes.string.isRequired,
  }).isRequired,
  boardId: PropTypes.string.isRequired,
  threadId: PropTypes.number.isRequired,
  attachment: attachmentShape.isRequired,
};

export default withStyles(styles)(PostAttachments);
