import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import PostAttachments from './post_attachments';

import { post as postShape } from '../../shapes';

const styles = () => ({
  postContainer: {
    marginBottom: 10,
  },
  postMeta: {
    fontSize: '0.8em',
    marginRight: '1em',
  },
});

const cutThreadTitle = (title) => {
  if (title.length < 100) {
    return title;
  }

  return `${title.slice(0, 100)} ...`;
};

const Post = ({
  classes,
  boardId,
  threadRemainedPosts,
  post,
  indexInThread,
  isPreview = false,
}) => (
  <div className={classes.postContainer}>
    {post.title !== undefined ? (
      <Typography align="justify" variant="h6">
        {cutThreadTitle(post.title)}
      </Typography>
    ) : null}
    <div>
      <Typography className={classes.postMeta} inline variant="caption">
        {`${new Date(post.created_at).toLocaleDateString()}
        ${new Date(post.created_at).toLocaleTimeString()}`}
      </Typography>
      <Typography className={classes.postMeta} inline variant="caption">
        {`#${post.id}`}
      </Typography>
      {isPreview ? null : (
        <Typography className={classes.postMeta} inline variant="caption">
          {indexInThread}
        </Typography>
      )}
      {indexInThread === 1 && isPreview ? (
        <Typography className={classes.postMeta} inline variant="caption">
          {`Remained posts: ${threadRemainedPosts}`}
        </Typography>
      ) : null}
      {indexInThread === 1 && isPreview ? (
        <Link to={`/${boardId}/threads/${post.thread_id}`}>
          <Typography className={classes.postMeta} inline variant="caption">
            Open
          </Typography>
        </Link>
      ) : null}
    </div>

    {post.attachments.length > 0 ? (
      <PostAttachments boardId={boardId} threadId={post.thread_id} attachments={post.attachments} />
    ) : null}

    {post.text !== undefined
      ? post.text.split('\n').map((line) => (
        <Typography align="justify" variant="body1">
          {line}
        </Typography>
      ))
      : null}
  </div>
);

Post.defaultProps = {
  isPreview: false,
  threadRemainedPosts: 0,
};

Post.propTypes = {
  classes: PropTypes.shape({
    postMeta: PropTypes.string.isRequired,
  }).isRequired,
  isPreview: PropTypes.bool,
  indexInThread: PropTypes.number.isRequired,
  boardId: PropTypes.string.isRequired,
  threadRemainedPosts: PropTypes.number,
  post: postShape.isRequired,
};

export default withStyles(styles)(Post);
