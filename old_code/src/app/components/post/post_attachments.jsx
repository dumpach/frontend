import React from 'react';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';

import Attachment from './attachment';

import { attachment as attachmentShape } from '../../shapes';

const styles = () => ({
  attachmentsContainer: {
    margin: '20px 0',
  },
});

const PostAttachments = ({
  attachments, boardId, threadId, classes,
}) => (
  <Grid
    container
    className={classes.attachmentsContainer}
    justify="space-around"
    alignItems="center"
  >
    {attachments.map((attachment) => (
      <Attachment
        key={attachment.id}
        boardId={boardId}
        threadId={threadId}
        attachment={attachment}
      />
    ))}
  </Grid>
);

PostAttachments.propTypes = {
  classes: PropTypes.shape({
    attachmentsContainer: PropTypes.string.isRequired,
  }).isRequired,
  boardId: PropTypes.string.isRequired,
  threadId: PropTypes.number.isRequired,
  attachments: PropTypes.arrayOf(attachmentShape).isRequired,
};

export default withStyles(styles)(PostAttachments);
