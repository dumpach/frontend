import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({
  paginationContainer: {
    marginBottom: 20,
  },
  linkDefault: {
    color: theme.palette.secondary.main,
    textDecoration: 'none',
  },
  linkActive: {
    fontWeight: 900,
  },
});

const makePagesArray = (count, threadsPerPage) => {
  const pagesCount = Math.ceil(count / threadsPerPage);
  const pagesArray = [];

  for (let i = 1; i <= pagesCount; i += 1) {
    pagesArray.push(i);
  }

  return pagesArray;
};

const Pagination = ({
  boardId, pageId, count, threadsPerPage, classes,
}) => {
  const pages = makePagesArray(count, threadsPerPage);

  return (
    <Grid
      container
      className={classes.paginationContainer}
      direction="row"
      justify="center"
      alignItems="center"
    >
      {pages.map((page, index) => (
        <Typography key={page} style={{ marginRight: index + 1 !== pages.length ? 10 : 0 }}>
          <NavLink
            className={
              pageId === '1' && page === 1
                ? `${classes.linkDefault} ${classes.linkActive}`
                : classes.linkDefault
            }
            activeClassName={classes.linkActive}
            to={`/${boardId}/${page}`}
          >
            {page}
          </NavLink>
        </Typography>
      ))}
    </Grid>
  );
};

Pagination.defaultProps = {
  pageId: '1',
};

Pagination.propTypes = {
  boardId: PropTypes.string.isRequired,
  pageId: PropTypes.string,
  count: PropTypes.number.isRequired,
  threadsPerPage: PropTypes.number.isRequired,
  classes: PropTypes.shape({
    linkDefault: PropTypes.string.isRequired,
    linkActive: PropTypes.string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(Pagination);
