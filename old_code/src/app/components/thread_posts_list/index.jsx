import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

import Post from '../post';

import { thread as threadShape } from '../../shapes';

const styles = (theme) => ({
  threadPostsListContainer: {
    [theme.breakpoints.up('xs')]: {
      margin: '0 10px',
    },
    [theme.breakpoints.up('sm')]: {
      margin: '0 10%',
    },
    [theme.breakpoints.up('md')]: {
      margin: '0 20%',
    },
    [theme.breakpoints.up('lg')]: {
      margin: '0 20%',
    },
    [theme.breakpoints.up('xl')]: {
      margin: '0 30%',
    },
  },
});

const ThreadPostsList = ({ thread, classes }) => (
  <div
    className={classes.threadPostsListContainer}
  >
    {thread.posts.map((post, index) => (
      <Post
        post={post}
        boardId={thread.board_id}
        key={post.id}
        indexInThread={index + 1}
      />
    ))}
  </div>
);

ThreadPostsList.propTypes = {
  classes: PropTypes.shape({
    threadPostsListContainer: PropTypes.string.isRequired,
  }).isRequired,
  thread: threadShape.isRequired,
};

export default withStyles(styles)(ThreadPostsList);
