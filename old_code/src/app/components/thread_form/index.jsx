import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
// import Checkbox from '@material-ui/core/Checkbox';
import Dropzone from 'react-dropzone';

import AttachFileIcon from '@material-ui/icons/AttachFile';
import SendIcon from '@material-ui/icons/Send';

import DropzoneAttachments from './dropzone_attachments';

import { createThread, updateThread } from '../../../store/thunks/thread';

const styles = (theme) => ({
  formContainer: {
    [theme.breakpoints.up('xs')]: {
      margin: '0 10px 20px',
    },
    [theme.breakpoints.up('sm')]: {
      margin: '0 10% 20px',
    },
    [theme.breakpoints.up('md')]: {
      margin: '0 20% 20px',
    },
    [theme.breakpoints.up('lg')]: {
      margin: '0 20% 20px',
    },
    [theme.breakpoints.up('xl')]: {
      margin: '0 30% 20px',
    },
  },
  displayNone: {
    display: 'none',
  },
  dropZoneContainer: {
    width: 'initial',
    minHeight: 150,
    borderColor: 'rgba(0, 0, 0, 0)',
    marginTop: 20,
    marginBottom: 20,
    cursor: 'pointer',
  },
  dropzoneAttachmentIconContainer: {
    minHeight: 150,
  },
  dropzoneAttachmentIcon: {
    color: theme.palette.text.hint,
    fontSize: '4em',
  },
  sendIcon: {
    color: theme.palette.text.hint,
  },
});

class ThreadForm extends PureComponent {
  state = {
    title: '',
    text: '',
    isSage: false,
    attachments: [],
    attachmentsPreviews: [],
  };

  componentWillUnmount() {
    const { attachmentsPreviews } = this.state;
    attachmentsPreviews.forEach((attachment) => URL.revokeObjectURL(attachment.preview));
  }

  onInputChange = (event, name) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  onChecboxChange = (event, name) => {
    this.setState({
      [name]: event.target.checked,
    });
  };

  onAttachmentsDrop = (acceptedFiles) => {
    this.setState({
      attachmentsPreviews: acceptedFiles.map((file) => ({
        preview: URL.createObjectURL(file),
        name: file.name,
      })),
      attachments: acceptedFiles,
    });
  };

  onRemoveAttachment = (event, name) => {
    event.stopPropagation();

    const { attachments, attachmentsPreviews } = this.state;

    URL.revokeObjectURL(
      attachmentsPreviews.find((attachmentPreview) => attachmentPreview.name === name).preview,
    );

    this.setState({
      attachments: attachments.filter((attachment) => attachment.name !== name),
      attachmentsPreviews: attachmentsPreviews.filter(
        (attachmentPreview) => attachmentPreview.name !== name,
      ),
    });
  };

  onSend = async () => {
    const { boardId, threadId, newThread } = this.props;

    if (newThread) {
      /* eslint-disable react/destructuring-assignment */
      await this.props.createThread(boardId, this.state, this.clearForm);
    } else {
      await this.props.updateThread(boardId, threadId, this.state, this.clearForm);
      /* eslint-enable react/destructuring-assignment */
    }
  };

  clearForm = () => {
    const { attachmentsPreviews } = this.state;
    attachmentsPreviews.forEach((attachment) => URL.revokeObjectURL(attachment.preview));

    this.setState({
      title: '',
      text: '',
      isSage: false,
      attachments: [],
      attachmentsPreviews: [],
    });
  };

  render() {
    const {
      title,
      text,
      //  isSage,
      attachmentsPreviews,
    } = this.state;
    const {
      classes,
      // newThread,
      isOpened,
    } = this.props;

    return (
      <div className={isOpened ? classes.formContainer : classes.displayNone}>
        <TextField
          id="title"
          label="title"
          value={title}
          fullWidth
          onChange={(event) => this.onInputChange(event, 'title')}
          margin="normal"
        />
        <TextField
          id="text"
          label="text"
          value={text}
          fullWidth
          multiline
          onChange={(event) => this.onInputChange(event, 'text')}
          margin="normal"
        />
        <Dropzone onDrop={this.onAttachmentsDrop}>
          {({ getRootProps, getInputProps }) => (
            <section>
              <div {...getRootProps()} className={classes.dropZoneContainer}>
                <input {...getInputProps()} />

                {attachmentsPreviews.length > 0 ? (
                  <DropzoneAttachments
                    attachments={attachmentsPreviews}
                    onRemoveAttachment={this.onRemoveAttachment}
                  />
                ) : (
                  <Grid
                    className={classes.dropzoneAttachmentIconContainer}
                    container
                    justify="center"
                    alignItems="center"
                  >
                    <AttachFileIcon className={classes.dropzoneAttachmentIcon} />
                  </Grid>
                )}
              </div>
            </section>
          )}
        </Dropzone>
        <Grid container justify="flex-end" alignItems="center">
          <IconButton onClick={this.onSend}>
            <SendIcon aria-label="create thread" className={classes.sendIcon} />
          </IconButton>
        </Grid>
      </div>
    );
  }
}

ThreadForm.defaultProps = {
  newThread: false,
  threadId: null,
};

ThreadForm.propTypes = {
  classes: PropTypes.shape({
    formContainer: PropTypes.string.isRequired,
    displayNone: PropTypes.string.isRequired,
    dropZoneContainer: PropTypes.string.isRequired,
    dropzoneAttachmentIconContainer: PropTypes.string.isRequired,
    dropzoneAttachmentIcon: PropTypes.string.isRequired,
  }).isRequired,
  boardId: PropTypes.string.isRequired,
  threadId: PropTypes.string,
  newThread: PropTypes.bool,
  createThread: PropTypes.func.isRequired,
  updateThread: PropTypes.func.isRequired,
  isOpened: PropTypes.bool.isRequired,
};

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ createThread, updateThread }, dispatch);

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ThreadForm),
);
