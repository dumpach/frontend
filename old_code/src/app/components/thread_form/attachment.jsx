import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import HighlightOffIcon from '@material-ui/icons/HighlightOff';

const styles = (theme) => ({
  attachmentContainer: {
    position: 'relative',
  },
  attachment: {
    maxHeight: 100,
    maxWidth: '100%',
    marginBottom: 10,
  },
  attachmentControlsContainer: {
    width: '100%',
    height: 25,
    backgroundColor: theme.palette.text.hint,
    position: 'absolute',
    bottom: 14,
  },
  attachmentControlRemoveIcon: {
    cursor: 'pointer',
    color: theme.palette.primary.main,
    fontSize: '1.2em',
  },
});

const PostAttachments = ({ attachment, classes, onRemoveAttachment }) => (
  <div className={classes.attachmentContainer}>
    <img className={classes.attachment} src={attachment.preview} alt={attachment.name} />
    <Grid
      container
      className={classes.attachmentControlsContainer}
      justify="space-around"
      alignItems="center"
    >
      <HighlightOffIcon
        className={classes.attachmentControlRemoveIcon}
        onClick={(event) => onRemoveAttachment(event, attachment.name)}
      />
    </Grid>
  </div>
);

PostAttachments.propTypes = {
  classes: PropTypes.shape({
    attachmentContainer: PropTypes.string.isRequired,
  }).isRequired,
  attachment: PropTypes.shape({
    name: PropTypes.string.isRequired,
    preview: PropTypes.string.isRequired,
  }).isRequired,
  onRemoveAttachment: PropTypes.func.isRequired,
};

export default withStyles(styles)(PostAttachments);
