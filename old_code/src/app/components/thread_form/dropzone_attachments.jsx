import React from 'react';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';

import Attachment from './attachment';

const PostAttachments = ({
  attachments, onRemoveAttachment,
}) => (
  <Grid
    container
    justify="space-between"
    alignItems="center"
  >
    {attachments.map((attachment) => (
      <Attachment
        key={attachment.preview}
        attachment={attachment}
        onRemoveAttachment={onRemoveAttachment}
      />
    ))}
  </Grid>
);

PostAttachments.propTypes = {
  attachments: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    preview: PropTypes.string.isRequired,
  })).isRequired,
  onRemoveAttachment: PropTypes.func.isRequired,
};

export default PostAttachments;
