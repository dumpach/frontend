import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

import HomeIcon from '@material-ui/icons/Home';
import SettingsIcon from '@material-ui/icons/Settings';

const styles = {
  drawerList: {
    minWidth: 200,
  },
  navLink: {
    textDecoration: 'none',
  },
};

const links = [
  { to: '/', icon: <HomeIcon />, text: 'Home' },
  { to: '/settings', icon: <SettingsIcon />, text: 'Settings' },
];

const DrawerMenu = ({ isOpen, handleToggle, classes }) => (
  <SwipeableDrawer
    open={isOpen}
    onOpen={handleToggle}
    onClose={handleToggle}
    elevation={0}
    hysteresis={0.2}
  >
    <div tabIndex={-1} role="button" onKeyDown={() => null} onClick={handleToggle}>
      <List className={classes.drawerList} component="nav">
        {links.map((link) => (
          <NavLink key={link.to} tabIndex={0} to={link.to} className={classes.navLink}>
            <ListItem button tabIndex={-1}>
              <ListItemIcon>{link.icon}</ListItemIcon>

              <ListItemText inset primary={link.text} />
            </ListItem>
          </NavLink>
        ))}
      </List>
    </div>
  </SwipeableDrawer>
);

DrawerMenu.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  handleToggle: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    drawerList: PropTypes.string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(DrawerMenu);
