/* eslint-disable react/jsx-filename-extension */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { frontloadConnect } from 'react-frontload';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
/* eslint-disable-next-line import/no-extraneous-dependencies */
import { withRouter } from 'react-router';

import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import Snackbar from '@material-ui/core/Snackbar';

import Routes from './routes';
import DrawerMenu from './components/drawer_menu';

import { getBoards } from '../store/thunks/boards';
import { hideNotification } from '../store/thunks/notification';

import { section as sectionShape } from './shapes';

const frontload = async (props) => {
  const { boards } = props;

  if (boards.data.length === 0) {
    return props.getBoards();
  }

  return null;
};

class App extends PureComponent {
  state = {
    isMenuOpen: false,
  };

  handleNotificationClose = () => {
    /* eslint-disable-next-line react/destructuring-assignment */
    this.props.hideNotification();
  };

  handleMenuToggle = () => {
    const { isMenuOpen } = this.state;

    this.setState({ isMenuOpen: !isMenuOpen });
  };

  render() {
    const { notification } = this.props;
    const { isMenuOpen } = this.state;

    return (
      <div className="main-page-container">
        <CssBaseline />
        <AppBar
          position="sticky"
          style={{ backgroundColor: 'rgba(255,255,255,0)', boxShadow: 'none' }}
        >
          <Toolbar disableGutters>
            <IconButton
              aria-label="open drawer"
              style={{ marginLeft: 8 }}
              onClick={this.handleMenuToggle}
            >
              <MenuIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <DrawerMenu isOpen={isMenuOpen} handleToggle={this.handleMenuToggle} />
        <Routes />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          open={notification.isOpen}
          autoHideDuration={6000}
          onClose={this.handleNotificationClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{notification.message}</span>}
        />
      </div>
    );
  }
}

App.propTypes = {
  boards: PropTypes.shape({
    data: PropTypes.arrayOf(sectionShape),
    isFetching: PropTypes.bool.isRequired,
  }).isRequired,
  hideNotification: PropTypes.func.isRequired,
  notification: PropTypes.shape({
    isOpen: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ boards, notification }) => ({
  boards,
  notification,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getBoards,
      hideNotification,
    },
    dispatch,
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(
    frontloadConnect(frontload, {
      onMount: true,
      onUpdate: false,
    })(App),
  ),
);
/* eslint-enable react/jsx-filename-extension */
