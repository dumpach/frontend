import cookies from './cookies';
import strings from './strings';

export { cookies, strings };
