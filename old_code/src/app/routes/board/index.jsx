import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { frontloadConnect } from 'react-frontload';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';

import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

import Page from '../../components/page';
import ThreadForm from '../../components/thread_form';
import Pagination from '../../components/pagination';
import ThreadPreview from '../../components/thread_preview';

import { getThreads } from '../../../store/thunks/threads';

import { thread as threadShape } from '../../shapes';

const styles = (theme) => ({
  formIconContainer: { margin: '20px 0' },
  formIcon: { color: theme.palette.text.hint, fontSize: '2em', cursor: 'pointer' },
});

const frontload = async (props) => {
  const {
    threads,
    match: {
      params: { boardId, pageId },
    },
    settings: { paginationThreadsPerPage },
  } = props;

  if (threads.data.length === 0) {
    return props.getThreads(
      boardId,
      paginationThreadsPerPage,
      (pageId - 1) * paginationThreadsPerPage || 0,
    );
  }

  if (threads.data[0].board_id !== boardId) {
    return props.getThreads(
      boardId,
      paginationThreadsPerPage,
      (pageId - 1) * paginationThreadsPerPage || 0,
    );
  }

  return null;
};

class Board extends PureComponent {
  state = {
    isFormOpened: false,
  };

  componentDidUpdate = (prevProps) => {
    const {
      match: {
        params: { boardId, pageId },
      },
      settings: { paginationThreadsPerPage },
    } = this.props;

    const {
      match: {
        params: { pageId: previousPageId },
      },
    } = prevProps;

    if (pageId !== previousPageId) {
      /* eslint-disable-next-line react/destructuring-assignment */
      this.props.getThreads(
        boardId,
        paginationThreadsPerPage,
        (pageId - 1) * paginationThreadsPerPage || 0,
      );
    }
  };

  handleToggleThreadFormClick = () => {
    const { isFormOpened } = this.state;
    this.setState({ isFormOpened: !isFormOpened });
  };

  render() {
    const { isFormOpened } = this.state;
    const { threads, settings, classes } = this.props;

    const {
      match: {
        params: { boardId, pageId },
      },
    } = this.props;

    // TODO: add EmptyThreads component, if requested page, that has no threads
    return (
      <Page id="board" title={boardId} description={`${boardId} board.`}>
        <Grid container direction="column" justify="center" alignItems="center">
          <Typography variant="h2">
            {'Board: '}
            {boardId}
          </Typography>
        </Grid>
        <Grid
          container
          className={classes.formIconContainer}
          direction="column"
          justify="center"
          alignItems="center"
        >
          <IconButton aria-label="open form" onClick={this.handleToggleThreadFormClick}>
            {isFormOpened ? (
              <RemoveIcon className={classes.formIcon} />
            ) : (
              <AddIcon className={classes.formIcon} />
            )}
          </IconButton>
        </Grid>
        <ThreadForm boardId={boardId} isOpened={isFormOpened} newThread />
        <Pagination
          boardId={boardId}
          pageId={pageId}
          count={threads.count}
          threadsPerPage={settings.paginationThreadsPerPage}
        />
        <Grid container direction="column" justify="center" alignItems="stretch">
          {threads.data.map((thread, index) => (
            <ThreadPreview
              key={thread.board_id + thread.id}
              thread={thread}
              isLastThreadInList={index + 1 === threads.data.length}
            />
          ))}
        </Grid>
        {threads.data.length !== 0 ? (
          <Pagination
            boardId={boardId}
            pageId={pageId}
            count={threads.count}
            threadsPerPage={settings.paginationThreadsPerPage}
          />
        ) : null}
      </Page>
    );
  }
}

Board.propTypes = {
  getThreads: PropTypes.func.isRequired,
  settings: PropTypes.shape({
    paginationThreadsPerPage: PropTypes.number.isRequired,
  }).isRequired,
  threads: PropTypes.shape({
    count: PropTypes.number.isRequired,
    isFetching: PropTypes.bool.isRequired,
    isLastPage: PropTypes.bool.isRequired,
    data: PropTypes.arrayOf(threadShape).isRequired,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      boardId: PropTypes.string.isRequired,
      pageId: PropTypes.string,
    }).isRequired,
  }).isRequired,
  classes: PropTypes.shape({
    formIconContainer: PropTypes.string.isRequired,
    formIcon: PropTypes.string.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ settings, threads }) => ({
  settings,
  threads,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ getThreads, push }, dispatch);

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(
    frontloadConnect(frontload, {
      onMount: true,
      onUpdate: false,
    })(Board),
  ),
);
