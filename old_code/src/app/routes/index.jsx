import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import NotFound from './not-found';

const Homepage = Loadable({
  loader: () => import(/* webpackChunkName: "homepage" */ './homepage'),
  loading: () => null,
  modules: ['homepage'],
});

const Settings = Loadable({
  loader: () => import(/* webpackChunkName: "settings" */ './settings'),
  loading: () => null,
  modules: ['settings'],
});

const Board = Loadable({
  loader: () => import(/* webpackChunkName: "board" */ './board'),
  loading: () => null,
  modules: ['board'],
});

const Thread = Loadable({
  loader: () => import(/* webpackChunkName: "board" */ './thread'),
  loading: () => null,
  modules: ['thread'],
});

const Routes = () => (
  <Switch>
    <Route exact path="/" component={Homepage} />

    <Route exact path="/settings" component={Settings} />

    <Route exact path="/:boardId/" component={Board} />
    <Route exact path="/:boardId/:pageId" component={Board} />
    <Route exact path="/:boardId/threads/:threadId" component={Thread} />

    <Route component={NotFound} />
  </Switch>
);

export default Routes;
