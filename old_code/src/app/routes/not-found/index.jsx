import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Page from '../../components/page';

const styles = () => ({
  notFoundText: {
    fontSize: '2em',
    textAlign: 'center',
  },
});

export default withStyles(styles)(({ classes }) => (
  <Page id="not-found" title="Not Found" description="This is embarrassing." noCrawl>
    <Typography className={classes.notFoundText}>Sorry, nothing to do here.</Typography>
  </Page>
));
