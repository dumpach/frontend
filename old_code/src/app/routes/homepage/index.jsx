import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import Page from '../../components/page';

import { section as sectionShape } from '../../shapes';

const HomePage = ({ sections }) => (
  <Page id="homepage" title="Homepage" description="Homepage of shit.">
    <Grid container direction="column" justify="center" alignItems="center">
      <Typography variant="h2">Homepage</Typography>
      {sections.map((section) => (
        <div style={{ marginTop: '2em' }} key={section.title}>
          <Typography variant="h3">{section.title}</Typography>

          {section.boards.map((board) => (
            <Link style={{ textDecoration: 'none' }} key={board.id} to={board.id}>
              <Typography variant="h4">
                {board.id}
                {' '}
-
                {' '}
                {board.title}
              </Typography>
            </Link>
          ))}
        </div>
      ))}
    </Grid>
  </Page>
);

HomePage.propTypes = {
  sections: PropTypes.arrayOf(sectionShape).isRequired,
};

const mapStateToProps = ({ boards }) => ({
  sections: boards.data,
});

export default connect(
  mapStateToProps,
  {},
)(HomePage);
