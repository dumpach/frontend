import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { frontloadConnect } from 'react-frontload';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

import Page from '../../components/page';
import ThreadForm from '../../components/thread_form';
import ThreadPostsList from '../../components/thread_posts_list';

import { getThread } from '../../../store/thunks/thread';

import { thread as threadShape } from '../../shapes';

const styles = (theme) => ({
  threadContainer: {
    textAlign: 'center',
  },
  formIconContainer: { margin: '20px 0' },
  formIcon: { color: theme.palette.text.hint, fontSize: '2em', cursor: 'pointer' },
});

const frontload = async (props) => {
  const {
    thread,
    match: {
      params: { boardId, threadId },
    },
  } = props;

  if (thread.data.id === undefined) {
    return props.getThread(boardId, threadId);
  }

  if (thread.data.id !== parseInt(threadId, 10)) {
    return props.getThread(boardId, threadId);
  }

  return null;
};

class Thread extends Component {
  state = {
    isFormOpened: false,
  };

  componentDidUpdate = (prevProps) => {
    const {
      match: {
        params: { boardId, threadId },
      },
    } = this.props;
    const { boardId: prevBoardId, threadId: prevThreadId } = prevProps.match.params;

    if (boardId !== prevBoardId || threadId !== prevThreadId) {
      /* eslint-disable-next-line react/destructuring-assignment */
      this.props.getThread(boardId, threadId);
    }
  };

  handleToggleThreadFormClick = () => {
    const { isFormOpened } = this.state;
    this.setState({ isFormOpened: !isFormOpened });
  };

  render() {
    const { isFormOpened } = this.state;
    const { thread, classes } = this.props;
    const {
      match: {
        params: { boardId, threadId },
      },
    } = this.props;

    const seoTitle = thread.data.posts !== undefined ? thread.data.posts[0].text : null;
    const seoDescription = thread.data.posts !== undefined ? thread.data.posts[0].text : null;

    return (
      <Page id="thread" title={seoTitle} description={seoDescription}>
        <Grid
          container
          className={classes.threadContainer}
          direction="column"
          justify="center"
          alignItems="center"
        >
          <Typography variant="h2">
            Board:
            {' '}
            {boardId}
            {' '}
Thread:
            {' '}
            {threadId}
          </Typography>
        </Grid>

        <Grid
          container
          className={classes.formIconContainer}
          direction="column"
          justify="center"
          alignItems="center"
        >
          <IconButton aria-label="open form" onClick={this.handleToggleThreadFormClick}>
            {isFormOpened ? (
              <RemoveIcon className={classes.formIcon} />
            ) : (
              <AddIcon className={classes.formIcon} />
            )}
          </IconButton>
        </Grid>

        <ThreadForm boardId={boardId} threadId={threadId} isOpened={isFormOpened} />

        {thread.data.posts !== undefined ? (
          <Grid container direction="column" justify="center" alignItems="stretch">
            <ThreadPostsList key={thread.board_id + thread.id} thread={thread.data} />
          </Grid>
        ) : null}
      </Page>
    );
  }
}

// Thread.defaultProps = {
//   thread: {
//     isFetching: false,
//     data: {},
//   },
// };

Thread.propTypes = {
  getThread: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      boardId: PropTypes.string.isRequired,
      threadId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  thread: PropTypes.shape({
    isFetching: PropTypes.bool.isRequired,
    data: threadShape,
  }).isRequired,
  classes: PropTypes.shape({
    formIconContainer: PropTypes.string.isRequired,
    formIcon: PropTypes.string.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ thread }) => ({
  thread,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ getThread }, dispatch);

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(
    frontloadConnect(frontload, {
      onMount: true,
      onUpdate: false,
    })(Thread),
  ),
);
