import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import Page from '../../components/page';

import { updateTheme, updatePagination } from '../../../store/thunks/settings';
import { showNotification } from '../../../store/thunks/notification';

import { cookies } from '../../helpers';

class Settings extends PureComponent {
  handleThemeChange = (event) => {
    const {
      settings: { stylingTheme },
    } = this.props;

    if (stylingTheme !== event.target.value) {
      const date = new Date(new Date().getTime() + 60 * 60 * 24 * 1000 * 1000);
      cookies.setCookie('theme', event.target.value, { expires: date });
      /* eslint-disable react/destructuring-assignment */
      this.props.updateTheme(event.target.value);
      this.props.showNotification('Theme updated. Reload page in order to apply new theme.');
      /* eslint-enable react/destructuring-assignment */
    }
  };

  handlePaginationChange = (event) => {
    const {
      settings: { paginationThreadsPerPage },
    } = this.props;

    if (paginationThreadsPerPage !== event.target.value) {
      const date = new Date(new Date().getTime() + 60 * 60 * 24 * 1000 * 1000);
      cookies.setCookie('pagination', event.target.value, { expires: date });
      /* eslint-disable react/destructuring-assignment */
      this.props.updatePagination(event.target.value);
      this.props.showNotification('Pagination updated.');
      /* eslint-enable react/destructuring-assignment */
    }
  };

  render() {
    const { settings } = this.props;
    const paginationVariants = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50];

    return (
      <Page id="settings" title="Settings" description="Tune this shit up" noCrawl>
        <Grid container direction="column" justify="center" alignItems="center">
          <Typography variant="h2">Settings</Typography>
          <FormControl style={{ minWidth: 200 }} margin="normal">
            <InputLabel htmlFor="theme-select">Theme</InputLabel>
            <Select
              value={settings.stylingTheme}
              onChange={this.handleThemeChange}
              inputProps={{
                name: 'theme',
                id: 'theme-select',
              }}
            >
              {settings.stylingThemes.map((theme) => (
                <MenuItem key={theme} value={theme}>
                  {theme}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl style={{ minWidth: 200 }} margin="normal">
            <InputLabel htmlFor="threads-per-page-select">Threads per page</InputLabel>
            <Select
              value={settings.paginationThreadsPerPage}
              onChange={this.handlePaginationChange}
              inputProps={{
                name: 'threads-per-page',
                id: 'threads-per-page-select',
              }}
            >
              {paginationVariants.map((variant) => (
                <MenuItem key={variant} value={variant}>
                  {variant}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
      </Page>
    );
  }
}

Settings.propTypes = {
  settings: PropTypes.shape({
    paginationThreadsPerPage: PropTypes.number.isRequired,
    stylingTheme: PropTypes.string.isRequired,
    stylingThemes: PropTypes.arrayOf(PropTypes.string).isRequired,
  }).isRequired,
  updateTheme: PropTypes.func.isRequired,
  updatePagination: PropTypes.func.isRequired,
  showNotification: PropTypes.func.isRequired,
};

const mapStateToProps = ({ settings }) => ({
  settings,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateTheme,
      updatePagination,
      showNotification,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Settings);
