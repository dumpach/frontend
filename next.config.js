module.exports = {
  distDir: './build', // Uses relative path from renderer dir option
  pageExtensions: ['jsx', 'js'],
  publicRuntimeConfig: {
    NODE_ENV: process.env.NODE_ENV,
    API_HOST: process.env.API_HOST,
    API_PORT: process.env.API_PORT,
  },
};
