# Frontend
Main frontend for Dumpach Imageboard. Build upon [NextJS](https://github.com/zeit/next.js/).

## Prepare
Clone some repos:
  - https://gitlab.com/dumpach/docker - main docker-compose files repo
  - https://gitlab.com/dumpach/backend - backend, lol
  - https://gitlab.com/dumpach/nginx - if you want try prodlike environment
  - this repo

## Run
### Development
Run from `docker` folder: 
```bash
docker-compose -f development.docker-compose.yml
```
It will start postgres, backend in development environment, frontend in development environment.

### Prodlike
Run from `docker` folder: 
```bash
docker-compose -f production.development.docker-compose.yml
```
It will start postgres, backend in production environment, build frontend in production environment and start renderer, nginx with dummy ssl certificates.